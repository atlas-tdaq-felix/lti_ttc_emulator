#
# This file is the temperature-logging recipe.
#

SUMMARY = "Simple temperature-logging application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"


SRC_URI = "file://get_temperature \
           file://log_temperature \
	"

S = "${WORKDIR}"
RDEPENDS:${PN} += "bash"

do_install() {
	    # install -d ${D}${bindir}
        # install -m 0755 ${WORKDIR}get_temperature ${D}/${bindir}
         #install -m 0755 ${WORKDIR}/init_cams ${D}${bindir}
        install -d ${D}${bindir}
        install -m 0755 ${WORKDIR}/get_temperature ${D}${bindir}/
        install -m 0755 ${WORKDIR}/log_temperature ${D}${bindir}/
}
