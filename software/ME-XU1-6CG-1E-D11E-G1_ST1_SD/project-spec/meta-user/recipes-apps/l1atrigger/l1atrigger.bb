#
# This file is the l1atrigger recipe.
#

SUMMARY = "Simple l1atrigger application"
SECTION = "PETALINUX/apps"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM ?= "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"
#SRCREV = "2690e1cbff954f40fd3eee2314860115960ca2b5"
SRCREV = "fbad7da4519bd8e0ee3815738c4dbb1be8723dc8"
TARGET_CXX_ARCH += "${LDFLAGS}"

SRC_URI = "git://gitlab.cern.ch/mleguijt/zedtrigger.git;protocol=https;"

S = "${WORKDIR}/git"

do_compile() {
	     oe_runmake -f Makefile-zed
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 zedtrigger ${D}${bindir}
}
