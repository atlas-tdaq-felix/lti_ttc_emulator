# LTI Emulator

A minimal LTI system based on an Enclustra Mercury+ XU1 module and Mercury+ ST1 carrier board to be used as an LTI source in a lab setup.

A presentation can be found [here](https://indico.cern.ch/event/1302023/contributions/5474781/attachments/2678484/4646796/ttc-lti-generator.pdf).

## Requirements

### Hardware
The following hardware requirements are:
- [FPGA](https://www.enclustra.com/en/products/system-on-chip-modules/mercury-xu1/) (make sure to include the heat sink)
- [Baseboard](https://www.enclustra.com/en/products/base-boards/mercury-st1/) (Optional power supply)
- Placed reference oscillator (SI570 @156.25MHz) on the ME-ST1 carrier board. (Indicated REF) 
  * The 100MHz reference oscillator connected to the SI5338B seems to give weird results. (Translated from Dutch: blowing tree crystal) 
#### Optional Hardware
- [Hightech Global FMC card](https://www.hitechglobal.com/FMCModules/FMC_4SFP+_Module.htm)

### Software
Currently building the project is done with:
- Vivado 2021.2
- Petalinux 2022.1

## Downloading the project files
```
git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/lti_ttc_emulator.git
```

## Building the project
After cloning the project:
```
cd firmware
```

Load the correct Vivado version (Project currently build on 2021.2).
```
source /eda/fpga/xilinx/Vivado/2021.2/settings64.sh
```

Open Vivado.
```
vivado
```

To create the project from scratch, use the following tcl script.
```
source ./scripts/create_project.tcl
```

Build the project
```
source ./scripts/do_implementation.tcl
```

## Building the Petalinux Image
Navigate to the Petalinux folder
```
cd ../software/ME-XU1-6CG-1E-D11E-G1_ST1_SD
```

Load the correct Petalinux version
```
source /eda/fpga/xilinx/Petalinux2022.1/settings.sh
```

To configure Petalinux use:
```
petalinux-config --get-hw-description ../../firmware/Vivado/ME-XU1-6CG-1E-D11E/
```

After configuration, use:
```
petalinux-build
```

Package the file
```
petalinux-package --boot --u-boot --fpga ../../firmware/Vivado/ME-XU1-6CG-1E-D11E/*.bit --force
```


## Partitioning the SD card
The following section describes how to prepare the SD card.


Using `dmesg |tail` we can find the SD card inserted in the PC.


**/DEV/SDA IS AN EXAMPLE ONLY! MAKE ABSOLUTELY SURE YOU ARE USING THE CORRECT DEVICE FOR THE COMMANDS IN THIS SECTION! IF YOU DON'T YOU MIGHT DESTROY FILES ON YOUR COMPUTER!**

When using fdisk, the program gives feedback when selecting an option. The following section displays:
`The command`
```
Feedback from fdisk
```

Use fdisk to create 2 partition
`sudo fdisk /dev/sda`

```
Welcome to fdisk (util-linux 2.31.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Command (m for help):
```

Delete the current partitions using **d**, if there are already partitions created, it should display the following lines.
`d`
```
Partition number (1,2, default 2):
```
Repeat this step until there are no partitions left.

Now create 2 new partitions:
`n`
```
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
```
`p`
```
Partition number (1-4, default 1):
```
`1`
```
First sector (2048-30375935, default 2048):
```
`2048`
```
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-30375935, default 30375935):
```
`526335`
```
Created a new partition 1 of type 'Linux' and of size 256 MiB.
Partition #1 contains a vfat signature.

Do you want to remove the signature? [Y]es/[N]o:
```
`y`

Repeat the steps for partition 2, we now use all the defaults:
```
Do you want to remove the signature? [Y]es/[N]o:
```
`y`

Use **p** to display the current partitions. It should look something like this:

```
Command (m for help): p

Disk /dev/sda: 14.48 GiB, 15552479232 bytes, 30375936 sectors
Disk model: SD/MMC CRW      
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xe018a0e5

Device     Boot  Start      End  Sectors  Size Id Type
/dev/sda1         2048   526335   524288  256M 83 Linux
/dev/sda2       526336 30375935 29849600 14.2G 83 Linux
```

Use `w` to write the changes.

Format the new partitions:
```
sudo mkfs.fat /dev/sda1
sudo mkfs.ext4 /dev/sda2
```


## Writing the Petalinux Image to an SD card
The files that need to be copied to the smaller `fat` (boot) partition are:
- BOOT.BIN
- boot.scr
- system.dtb
- Image

To copy the rootfs to the `ext4` (root) partition, we use the `.cpio` file.

`cd <SDcard/path>`

Use:
`cpio -idv < <path/to/petalinux>/rootfs.cpio`

This can also be done when the root file system is mounted through sshfs

## File downloads
The generated files from the master branch are available at:
https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/firmware/lti_ttc_emulator/

## Authors and acknowledgment
In Memory of the original creator of this project: Mesfin Gebyehu


## Further instructions
Please use these links for Petalinux documentation and tutorials:

- https://docs.xilinx.com/r/en-US/ug1144-petalinux-tools-reference-guide
- https://xilinx.github.io/Embedded-Design-Tutorials/docs/2022.2/build/html/docs/Introduction/Versal-EDT/Versal-EDT.html
- https://xilinx-wiki.atlassian.net/wiki/spaces/A/overview?homepageId=18844350

