--! This file is part of the lti ttc emulator firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/lti_ttc_emulator).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!               Melvin Leguijt
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
use ieee.std_logic_1164.all;

package lti_package is

    type array_std_logic_vector36_type is array (0 to 4) of std_logic_vector(35 downto 0);
    type array_std_logic_vector12_type is array (0 to 4) of std_logic_vector(11 downto 0);
    type array_std_logic_type is array (0 to 4) of std_logic;
    type array_std_logic2_type is array (0 to 1) of std_logic;
    type array_std_logic_vector0_type is array (0 to 4) of std_logic_vector(0 downto 0);
    type array_std_logic_vector32_type is array (0 to 4) of std_logic_vector(31 downto 0);
    type array_std_logic_vector4_type is array (0 to 4) of std_logic_vector(3 downto 0);
    type array_std_logic_vector8_type is array (0 to 4) of std_logic_vector(7 downto 0);
    type array_std_logic_vector16_type is array (0 to 4) of std_logic_vector(15 downto 0);
    type array_std_logic_vector38_type is array (0 to 4) of std_logic_vector(37 downto 0);
    type array_std_logic_vector2_type is array (0 to 4) of std_logic_vector(1 downto 0);

    type lti_data_kchar_reg_array is array (0 to 5) of std_logic_vector(35 downto 0);
    constant lti_data_kchar_init : lti_data_kchar_reg_array := ( -- address, val, mask, read)
        (X"089898989"),                 -- 0
        (X"0aaaaaaaa"),                 -- 1
        (X"055555555"),                 -- 2
        (X"0bbbbbbbb"),                 -- 3
        (X"066666666"),                 -- 4
        (X"18888" & X"50" & X"BC")      -- 5
    );

end package lti_package;
