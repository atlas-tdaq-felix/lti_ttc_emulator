library ieee, work;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.TTC709_pkg.all;

entity sim_lti_output is
  port (
    clk_240M   		: in     std_logic;
    ECR_push         : in     std_logic;
    BCR_push         : in     std_logic;
    busy_in          : in     std_logic;
    sys_rst_n		 : in     std_logic;
    trigger_push      : in     std_logic);
end entity sim_lti_output;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_i2c.structure
-- Version       : 1925 (Subversion)
-- Last modified : Thu Oct 16 14:06:55 2014.
--------------------------------------------------------------------------------

architecture structure of sim_lti_output is

	signal ECR_r   : std_logic;
	signal trigger_r   : std_logic;
	signal BCR_r   : std_logic;
	signal TTC_out_t   : std_logic;
	signal ready   : std_logic;
    signal BCR_PERIOD : std_logic_vector(31 downto 0) := (others => '0');   
    signal OCR_PERIOD : std_logic_vector(31 downto 0) := (others => '0');
    signal L1A_PERIOD : std_logic_vector(31 downto 0) := (others => '0');
    signal TTC_CONFIG : std_logic_vector(31 downto 0) := (others => '0');
    signal enb 			 : STD_LOGIC ;
    signal web              : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal addrb            : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal doutb            : STD_LOGIC_VECTOR(31 DOWNTO 0) := (others => '0');
   signal     L1A_COUNT_IN	 :  STD_LOGIC_VECTOR ( 31 downto 0 ) := (others => '0');
   signal    BCR_count_en  		: std_logic;
   signal    LUT_CONT_EN  		: std_logic;
signal L1A_COUNT_read		: STD_LOGIC_VECTOR ( 31 downto 0 );
signal L1A_AUTO_HALT 		: STD_LOGIC := '0';
signal L1A_AUTO_READY 		: STD_LOGIC;
signal BUSY_EN				: std_logic := '0';
signal BUSY_PULSE_COUNT		: STD_LOGIC_VECTOR ( 31 downto 0 );

signal BCR_PERIOD_COUNT : std_logic_vector(11 downto 0) := (others => '0');
signal L1A_COUNT		: std_logic_vector(37 downto 0) := (others => '0');
signal ORBITID_COUNT	: std_logic_vector(31 downto 0) := (others => '0');
signal LTIData_out 	: std_logic_vector(31 downto 0) := (others => '0');
signal Kchar_out  	: std_logic_vector(3 downto 0) := (others => '0');
signal reset : std_logic := '0';
signal  data_out                  : std_logic_vector(191 downto 0);
signal  clk40_out                 : std_logic;
signal  clk40_ready_out           : std_logic; --@clk240_in
signal  cnt_error_out             : std_logic_vector(2 downto 0);
signal  decoder_aligned_out       : std_logic;
signal  crc_valid_out             : std_logic;
signal LinkAligned_in			  : std_logic := '1';
signal data33_in					: std_logic_vector(32 downto 0);

signal LBID_0 			: STD_LOGIC_VECTOR ( 15 downto 0 ) := (others => '0');
signal GRST_0 			: STD_LOGIC := '0';
signal SL0ID_0 			: STD_LOGIC:= '0';
signal SORB_0	 		: STD_LOGIC:= '0';
signal SYNC_0 			: STD_LOGIC:= '0';
signal TRIGGERTYPE_0 	: STD_LOGIC_VECTOR ( 15 downto 0 ) := (others => '0');
signal ERRORFLAGS_0 	: STD_LOGIC_VECTOR ( 3 downto 0 ) := (others => '0');
signal TS_0 			: STD_LOGIC:= '0';
signal SYNCGLOBALDATA_0 : STD_LOGIC_VECTOR ( 15 downto 0 ) := (others => '0');
signal SYNCUSERDATA_0 	: STD_LOGIC_VECTOR ( 15 downto 0 ) := (others => '0');
signal PARTITION_0 		: STD_LOGIC_VECTOR ( 1 downto 0 ) := (others => '0');
signal PT_0 			: STD_LOGIC:= '0';

  --Byte0
signal PT_ila 				: std_logic := '0';
signal PARTITION_ila 		: std_logic_vector(1 downto 0) := "00";
signal BCID_ila 			: std_logic_vector(11 downto 0) := (OTHERS => '0');
signal SYNCUSERDATA_ila 	: std_logic_vector(15 downto 0) := (OTHERS => '0');

--Byte 1
signal SYNCGLOBALDATA_ila 	: std_logic_vector(15 downto 0) := (OTHERS => '0');
signal TS_ila 				: std_logic := '0';
signal ERRORFLAGS_ila 		: std_logic_vector(3 downto 0) := (OTHERS => '0');
signal SL0ID_ila 			: std_logic := '0';  -- set L0ID
signal SORB_ila 			: std_logic := '0';  -- set ORBITID
signal SYNC_ila 			: std_logic := '0';  
signal GRST_ila 			: std_logic := '0';
signal L0A_ila	 			: std_logic := '0';
signal L0ID_ila 			: std_logic_vector(37 downto 0) := (OTHERS => '0');
--Byte 2
--signal L0ID 			: std_logic_vector(37 downto 0) := (OTHERS => '0');
--Byte 3
signal ORBITID_ila 			: std_logic_vector(31 downto 0) := (OTHERS => '0');
--Byte 4  
signal TRIGGERTYPE_ila		: std_logic_vector(15 downto 0) := (OTHERS => '0');
signal LBID_ila				: std_logic_vector(15 downto 0) := (OTHERS => '0');


  component edge_det
	port (
      clk_160M    				: in     std_logic;
      sys_rst_n   				: in     std_logic;
      BCR_push    				: in     std_logic;
      ECR_push    				: in     std_logic;
      trigger_push 				: in     std_logic;
      busy_in     				: in     std_logic;
      ECR_r       				: out    std_logic;
      trigger_r    				: out    std_logic;
      BCR_r       				: out    std_logic;
      BCR_count_en				: in    std_logic;
	  BCR_PERIOD_IN	  		: in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
	  OCR_PERIOD_IN	  		: in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
      L1A_COUNT_IN          : in     STD_LOGIC_VECTOR ( 31 downto 0 );
	  L1A_PERIOD_IN	  		: in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
	  TTC_CONFIG_IN	  		: in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
	  LUT_CONT_EN			: in std_logic;
	  L1A_COUNT_read		: out 	 STD_LOGIC_VECTOR ( 31 downto 0 );
      L1A_AUTO_HALT 		: in STD_LOGIC;
      L1A_AUTO_READY 		: out STD_LOGIC;
      BUSY_EN				: in std_logic;
      BUSY_PULSE_COUNT		: out STD_LOGIC_VECTOR ( 31 downto 0 );
	  enb 					: OUT STD_LOGIC;
	  web 					: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	  addrb 				: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	  doutb 				: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
-- LTI signals
	  BCR_PERIOD_COUNT_OUT	: OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
	  L1A_COUNT_OUT			: OUT  STD_LOGIC_VECTOR(37 DOWNTO 0);
	  ORBITID_COUNT_OUT		: OUT  STD_LOGIC_VECTOR(31 DOWNTO 0)
	  );
  end component edge_det;

  component LTI_DATA_GEN
	port (
    clk		             : in     std_logic;
    sys_rst_n            : in     std_logic;
	ECR_r				 : in     std_logic;
	trigger_r			 : in     std_logic;
	BCR_r				 : in     std_logic;
	BCR_count_en         : out    std_logic;
	LTIData_out		     : out    std_logic_vector(31 downto 0);
	Kchar_out		     : out    std_logic_vector(3 downto 0);
    BCR_PERIOD_COUNT_IN	 : in STD_LOGIC_VECTOR(11 DOWNTO 0);
    L1A_COUNT_IN			: in  STD_LOGIC_VECTOR(37 DOWNTO 0);
    ORBITID_COUNT_IN		: in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    LBID_0 				: in STD_LOGIC_VECTOR ( 15 downto 0 );
    GRST_0 				: in STD_LOGIC;
    SL0ID_0 			: in STD_LOGIC;
    SORB_0	 			: in STD_LOGIC;
    SYNC_0 				: in STD_LOGIC;
    TRIGGERTYPE_0 		: in STD_LOGIC_VECTOR ( 15 downto 0 );
    ERRORFLAGS_0 		: in STD_LOGIC_VECTOR ( 3 downto 0 );
    TS_0 				: in STD_LOGIC;
    SYNCGLOBALDATA_0 	: in STD_LOGIC_VECTOR ( 15 downto 0 );
    SYNCUSERDATA_0 		: in STD_LOGIC_VECTOR ( 15 downto 0 );
    PARTITION_0 		: in STD_LOGIC_VECTOR ( 1 downto 0 );
    PT_0 				: in STD_LOGIC
    );
end component LTI_DATA_GEN;

component ltittc_decoder
    port
    (
        reset_in                  : in std_logic;
        clk240_in                 : in std_logic;
        --@clk240_in
        LinkAligned_in            : in std_logic;
        data_in                   : in std_logic_vector(32 downto 0);
        IsK_in					  : in std_logic_vector(3 downto 0);	
        --@clk40_out
        data_out                  : out std_logic_vector(191 downto 0);
        clk40_out                 : out std_logic;
        clk40_ready_out           : out std_logic; --@clk240_in
        cnt_error_out             : out std_logic_vector(2 downto 0);
        decoder_aligned_out       : out std_logic;
        crc_valid_out             : out std_logic
    );

end component ltittc_decoder;


begin

  reset <= not sys_rst_n;
  data33_in <= '0' & LTIData_out;

 
  LBID_ila 				<=  data_out(159 downto 144);
  TRIGGERTYPE_ila 		<=  data_out(143 downto 128);
  ORBITID_ila 			<=  data_out(127 downto 96);
  L0ID_ila(37 downto 6)	<=  data_out(95 downto 64);
  SYNCGLOBALDATA_ila 	<=  data_out(63 downto 48);
  TS_ila 				<=  data_out(47);
  ERRORFLAGS_ila 		<=  data_out(46 downto 43);
  SL0ID_ila 			<=  data_out(42);
  SORB_ila	 			<=	data_out(41);
  SYNC_ila 				<=	data_out(40);
  GRST_ila 				<=  data_out(39);
  L0A_ila				<=  data_out(38);		
  L0ID_ila(5 downto 0)	<=  data_out(37 downto 32);
  SYNCUSERDATA_ila	    <=  data_out(15 downto 0);
  BCID_ila 				<=  data_out(27 downto 16);
  PARTITION_ila 		<=  data_out(29 downto 28);
  PT_ila 				<=  data_out(30);
  
--    31    			typemsg_40        <= TTC_data_decoder_40(31+0*32);
--    30:28 			--partn_40          <= TTC_data_decoder_40(30+0*32 downto 28+0*32);
--y    27:16 			bcid_40           <= TTC_data_decoder_40(27+0*32 downto 16+0*32);
--y    15:0  			--syncusrdata_40    <= TTC_data_decoder_40(15+0*32 downto  0+0*32);
--y    63:48 			--syncglobaldata_40 <= TTC_data_decoder_40(31+1*32 downto 16+1*32);
--y    47    			--turnsignal_40     <= TTC_data_decoder_40(15+1*32);
--y    46:43 			--errorflags_40     <= TTC_data_decoder_40(14+1*32 downto 11+1*32);
--y    42:   			sl0id_40          <= TTC_data_decoder_40(10+1*32)                when typemsg_40 = '0' else '0';
--y    41    			sorb_40           <= TTC_data_decoder_40( 9+1*32)                when typemsg_40 = '0' else '0';
--y    40    			sync_40           <= TTC_data_decoder_40( 8+1*32)                when typemsg_40 = '0' else '0';
--y    39    			grst_40           <= TTC_data_decoder_40( 7+1*32)                when typemsg_40 = '0' else '0';
--y    38    			l0a_40            <= TTC_data_decoder_40( 6+1*32)                when typemsg_40 = '0' else '0';
--y    95:64,37:32    	l0id_40           <= TTC_data_decoder_40(31+2*32 downto  0+2*32) & TTC_data_decoder_40(5+1*32 downto 0+1*32)   when typemsg_40 = '0' else l0id_40_r;
--y    127:96    		orbitid_40        <= TTC_data_decoder_40(31+3*32 downto  0+3*32) when typemsg_40 = '0' else orbitid_40_r;
--x    159:144    		ttype_40          <= TTC_data_decoder_40(31+4*32 downto 16+4*32) when typemsg_40 = '0' else ttype_40_r;
--x    143:128    		lbid_40           <= TTC_data_decoder_40(15+4*32 downto  0+4*32) when typemsg_40 = '0' else lbid_40_r;
--No    159:96    		asyncusrdata_40   <= TTC_data_decoder_40(31+4*32 downto  0+3*32) when typemsg_40 = '1' else asyncusrdata_40_r;
--    191:176    		--crc_40            <= TTC_data_decoder_40(31+5*32 downto 16+5*32);
  
  
  sim_edge_comp: edge_det
	port map (                
       clk_160M     => clk_240M,		
	   sys_rst_n  =>   sys_rst_n,             
       ECR_r		=> ECR_r,		
       trigger_r	    => trigger_r,	
       ECR_push     => ECR_push,    
       BCR_push     => BCR_push,    
       busy_in      => busy_in,     
       BCR_count_en  => BCR_count_en,
       LUT_CONT_EN	=> LUT_CONT_EN,
       trigger_push  => trigger_push, 
	   BCR_PERIOD_IN  => BCR_PERIOD, 
	   OCR_PERIOD_IN  => OCR_PERIOD,
	   L1A_PERIOD_IN  => L1A_PERIOD,
	   L1A_COUNT_IN => L1A_COUNT_IN,
	   TTC_CONFIG_IN  => TTC_CONFIG,
       BCR_r		=> BCR_r,
       L1A_COUNT_read	=> L1A_COUNT_read,	
       L1A_AUTO_HALT 	=> L1A_AUTO_HALT, 	
       L1A_AUTO_READY 	=> L1A_AUTO_READY, 	
       BUSY_EN			=> BUSY_EN,			
       BUSY_PULSE_COUNT => BUSY_PULSE_COUNT,
       enb 			=> enb,
       web          => web,
       addrb        => addrb,
       doutb        => doutb,
       BCR_PERIOD_COUNT_OUT	=> BCR_PERIOD_COUNT,	
       L1A_COUNT_OUT		=> L1A_COUNT,				
       ORBITID_COUNT_OUT	=> ORBITID_COUNT	
       );		

  sim_LTI_comp: LTI_DATA_GEN
	port map ( 
	   clk		            => clk_240M,		            
	   sys_rst_n            => sys_rst_n,           
	   ECR_r				=> ECR_r,				
	   trigger_r			=> trigger_r,			
	   BCR_r				=> BCR_r,				
	   BCR_count_en         => BCR_count_en,        
	   LTIData_out		    => LTIData_out,		    
	   Kchar_out		    => Kchar_out,		    
	   BCR_PERIOD_COUNT_IN	=> BCR_PERIOD_COUNT,	
	   L1A_COUNT_IN		    => L1A_COUNT,		
	   ORBITID_COUNT_IN	    => ORBITID_COUNT,
	   LBID_0 				=> LBID_0,
	   GRST_0 				=> GRST_0,
	   SL0ID_0 				=> SL0ID_0,
	   SORB_0	 			=> SORB_0,
	   SYNC_0 				=> SYNC_0,
	   TRIGGERTYPE_0 		=> TRIGGERTYPE_0,
	   ERRORFLAGS_0 		=> ERRORFLAGS_0,
	   TS_0 				=> TS_0,
	   SYNCGLOBALDATA_0 	=> SYNCGLOBALDATA_0,
	   SYNCUSERDATA_0 		=> SYNCUSERDATA_0,
	   PARTITION_0 			=> PARTITION_0,
	   PT_0 				=> PT_0 	
	   );				

  sim_ltittc_decodercomp: ltittc_decoder
	port map ( 
		reset_in            => reset,           
		clk240_in           => clk_240M,          
		--@clk240_in        => --@clk240_in
		LinkAligned_in      => LinkAligned_in,     
		data_in             => data33_in, 
		IsK_in				=> Kchar_out,           
        --@clk40_out        => --@clk40_out
        data_out            => data_out,           
        clk40_out           => clk40_out,          
        clk40_ready_out     => clk40_ready_out,    
        cnt_error_out       => cnt_error_out,      
        decoder_aligned_out => decoder_aligned_out,
        crc_valid_out       => crc_valid_out      
	   );				

end architecture structure ; -- of 8t49n242_i2c
