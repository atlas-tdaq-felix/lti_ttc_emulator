library ieee, xil_defaultlib;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use xil_defaultlib.TTC709_pkg.all;

entity sim_ttc_output is
  port (
    clk_160M	     : in     std_logic;
    ECR_push         : in     std_logic;
    BCR_push         : in     std_logic;
    busy_in          : in     std_logic;
    sys_rst_n		 : in     std_logic;
    TTC_out			 : out    std_logic; 
    triger_push      : in     std_logic);
end entity sim_ttc_output;

--------------------------------------------------------------------------------
-- Object        : Architecture work.si570_i2c.structure
-- Version       : 1925 (Subversion)
-- Last modified : Thu Oct 16 14:06:55 2014.
--------------------------------------------------------------------------------

architecture structure of sim_ttc_output is

	signal ECR_r   : std_logic;
	signal triger_r   : std_logic;
	signal BCR_r   : std_logic;
	signal TTC_out_t   : std_logic;
	signal ready   : std_logic;
    signal BCR_PERIOD : std_logic_vector(31 downto 0) := (others => '0');   
    signal OCR_PERIOD : std_logic_vector(31 downto 0) := (others => '0');
    signal L1A_PERIOD : std_logic_vector(31 downto 0) := (others => '0');
    signal TTC_CONFIG : std_logic_vector(31 downto 0) := (others => '0');
    signal enb 			 : STD_LOGIC ;
    signal web              : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal addrb            : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal doutb            : STD_LOGIC_VECTOR(31 DOWNTO 0) := (others => '0');
   signal     L1A_COUNT_IN	 :  STD_LOGIC_VECTOR ( 31 downto 0 ) := (others => '0');
   signal    BCR_count_en  		: std_logic;
   signal    LUT_CONT_EN  		: std_logic;
signal L1A_COUNT_read		: STD_LOGIC_VECTOR ( 31 downto 0 );
signal L1A_AUTO_HALT 		: STD_LOGIC;
signal L1A_AUTO_READY 		: STD_LOGIC;
signal BUSY_EN				: std_logic;
signal BUSY_PULSE_COUNT		: STD_LOGIC_VECTOR ( 31 downto 0 );


  component edge_det
	port (
    clk_160M	     : in     std_logic;
    sys_rst_n            : in     std_logic;
	ECR_r			 : out    std_logic;
	trigger_r		 : out    std_logic;
    ECR_push         : in     std_logic;
    BCR_push         : in     std_logic;
    busy_in          : in     std_logic;
    BCR_count_en  		: in std_logic;
    LUT_CONT_EN  		: in std_logic;
    trigger_push      : in     std_logic;
    BCR_PERIOD_IN	 : in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
    OCR_PERIOD_IN	 : in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_PERIOD_IN	 : in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_COUNT_IN	 : in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
    TTC_CONFIG_IN	 : in 	 STD_LOGIC_VECTOR ( 31 downto 0 );
	BCR_r			 : out    std_logic;
	L1A_COUNT_read		: out 	 STD_LOGIC_VECTOR ( 31 downto 0 );
    L1A_AUTO_HALT 		: in STD_LOGIC;
    L1A_AUTO_READY 		: out STD_LOGIC;
    BUSY_EN				: in std_logic;
    BUSY_PULSE_COUNT		: out STD_LOGIC_VECTOR ( 31 downto 0 );
    enb 			 : OUT STD_LOGIC;
    web              : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    addrb            : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb            : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
  end component edge_det;

  component TTC_FSM
	port (
    clk_160M            : in     std_logic;
    sys_rst_n            : in     std_logic;
	ECR_r				 : in     std_logic;
    BCR_count_en  		: out std_logic;
	trigger_r			 : in     std_logic;
	BCR_r				 : in     std_logic;
	TTC_out			     : out    std_logic
  );
end component TTC_FSM;

COMPONENT TTC_BRAM_L1A
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;


component ttc_decoder_core
port 
(
    --RESET_N                             : in std_logic;
	--== cdr interface ==--
	cdrclk_in_locked					: in std_logic; --160 MHz clock				
	cdrclk_in							: in std_logic;				
	cdrdata_in 							: in std_logic;				
	--== ttc decoder output ==--
	single_bit_error    				: out std_logic;
	double_bit_error    				: out std_logic;
	communication_error 				: out std_logic;
	l1a									: out std_logic;
	channelB_o                          : out std_logic;
	brc_strobe							: out std_logic;
	add_strobe							: out std_logic;
	--TTDDDDDEB
	brc_t2								: out std_logic_vector(1 downto 0);  
	brc_d4								: out std_logic_vector(3 downto 0);
	brc_e								: out std_logic; 
	brc_b								: out std_logic; 
	--AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
	add_a14								: out std_logic_vector(13 downto 0);
	add_e								: out std_logic;
	add_s8								: out std_logic_vector(7 downto 0);
	add_d8								: out std_logic_vector(7 downto 0);
	--== ttc decoder aux flags ==--
	ready								: out std_logic;				
	div_nrst 							: out std_logic;	-- flag to align 40MHz clk phase that can be used for clock dividers
	ttc_clk_gated						: out std_logic	-- gated 40MHz clock, for comparison only
	--cdrclk_en                           : out std_logic     -- reduces the 160 MHz clock to 40 MHz clock
);
end component ttc_decoder_core;




begin

  sim_edge_comp: edge_det
	port map (                
       clk_160M     => clk_160M,		
	   sys_rst_n  =>   sys_rst_n,             
       ECR_r		=> ECR_r,		
       trigger_r	    => triger_r,	
       ECR_push     => ECR_push,    
       BCR_push     => BCR_push,    
       busy_in      => busy_in,     
       BCR_count_en  => BCR_count_en,
       LUT_CONT_EN	=> LUT_CONT_EN,
       trigger_push  => triger_push, 
	   BCR_PERIOD_IN  => BCR_PERIOD, 
	   OCR_PERIOD_IN  => OCR_PERIOD,
	   L1A_PERIOD_IN  => L1A_PERIOD,
	   L1A_COUNT_IN => L1A_COUNT_IN,
	   TTC_CONFIG_IN  => TTC_CONFIG,
       BCR_r		=> BCR_r,
       L1A_COUNT_read	=> L1A_COUNT_read,	
       L1A_AUTO_HALT 	=> L1A_AUTO_HALT, 	
       L1A_AUTO_READY 	=> L1A_AUTO_READY, 	
       BUSY_EN			=> BUSY_EN,			
       BUSY_PULSE_COUNT => BUSY_PULSE_COUNT,
       enb 			=> enb,
       web          => web,
       addrb        => addrb,
       doutb        => doutb
       );		

  sim_TTC_FSM_comp: TTC_FSM
	port map ( 
	   clk_160M   =>   clk_160M,                  
	   sys_rst_n  =>   sys_rst_n,             
	   ECR_r	  =>   ECR_r,				
	   trigger_r	  =>   triger_r,		
	   BCR_r	  =>   BCR_r,			
       BCR_count_en  => BCR_count_en,
	   TTC_out	  =>   TTC_out_t);				

TTC_out <= TTC_out_t;
sim_ttc_decoder_core: ttc_decoder_core
port map 
(
    --RESET_N                             : in std_logic;
	--== cdr interface ==--
	cdrclk_in_locked					=>  '1',
	cdrclk_in							=>  clk_160M,
	cdrdata_in 							=>  TTC_out_t,
	--== ttc decoder output ==--        =>  ,
	single_bit_error    				=>  open,
	double_bit_error    				=>  open,
	communication_error 				=>  open,
	l1a									=>  open,
	channelB_o                          =>  open,
	brc_strobe							=>  open,
	add_strobe							=>  open,
	--TTDDDDDEB                         =>  open,
	brc_t2								=>  open,
	brc_d4								=>  open,
	brc_e								=>  open,
	brc_b								=>  open,
	--AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD  =>  open,
	add_a14								=>  open,
	add_e								=>  open,
	add_s8								=>  open,
	add_d8								=>  open,
	--== ttc decoder aux flags ==--     =>  open,
	ready								=>  ready,
	div_nrst 							=>  open,
	ttc_clk_gated						=>  open
	--cdrclk_en                           : out std_logic     -- reduces the 160 MHz clock to 40 MHz clock
);


TTC_BRAM_L1A_inst: TTC_BRAM_L1A
port map 
(
	clka 		=> '0', 
	ena         => '0', 
	wea         => (others => '0'), 
	addra       => (others => '0'),
	dina        => (others => '0'), 
	douta       => open,
	clkb        => clk_160M, 
	enb         => enb, 
	web         => web, 
	addrb       => addrb(15 downto 0),
	dinb        => (others => '0'), 
	doutb       => doutb
);

end architecture structure ; -- of 8t49n242_i2c
