--! This file is part of the lti ttc emulator firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/lti_ttc_emulator).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!               Melvin Leguijt
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity LTI_DATA_RCV is
    port(
        clk240_in  : in  std_logic;
        reset_in   : in  std_logic;
        LTIData_in : in  std_logic_vector(15 downto 0);
        Kchar_in   : in  std_logic_vector(1 downto 0);
        
        busy       : out std_logic
    );
end entity LTI_DATA_RCV;

architecture structure of LTI_DATA_RCV is

    signal crc_en    : std_logic := '0';
    signal crc_reset : std_logic := '0';
    signal crc_in    : std_logic_vector(15 downto 0);
    signal cnt       : integer range 0 to 5;

    -- LTI-TTC data type
    signal crc_out       : std_logic_vector(15 downto 0) := (OTHERS => '0');
    constant Kchar_comma : std_logic_vector(7 DOWNTO 0)  := X"BC";

    -- End TTC data
    constant IDLE        : std_logic_vector(1 downto 0) := "00";
    constant COMMA_FOUND : std_logic_vector(1 downto 0) := "01";
    signal DETECTOR_st   : std_logic_vector(1 downto 0) := "00";

    --End states
    signal crc_valid_out : std_logic;
    signal IsK_in        : std_logic_vector(1 downto 0);
    signal data_out_i    : std_logic_vector(95 downto 0);
    signal cnt_i1a       : std_logic_vector(2 downto 0);
    signal LTIData_in_d0 : std_logic_vector(15 downto 0);

    -- Debug ila
    COMPONENT debug_data_rcv            -- @suppress "Component declaration 'debug_data_rcv' has none or multiple matching entity declarations"
        PORT(
            clk    : IN STD_LOGIC;
            probe0 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
            probe1 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
            probe2 : in STD_LOGIC_VECTOR(15 DOWNTO 0);
            probe3 : in STD_LOGIC_VECTOR(1 DOWNTO 0);
            probe4 : in STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe5 : in STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe6 : in STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe7 : in STD_LOGIC_VECTOR(1 DOWNTO 0);
            probe8 : in STD_LOGIC_VECTOR(2 DOWNTO 0);
            probe9 : in STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

begin

    ila0_data_rcv_inst : debug_data_rcv
        PORT MAP(
            clk       => clk240_in,
            probe0    => crc_out,
            probe1    => LTIData_in,
            probe2    => crc_in,
            probe3    => DETECTOR_st,
            probe4(0) => crc_reset,
            probe5(0) => crc_en,
            probe6(0) => crc_valid_out,
            probe7    => IsK_in,
            probe8    => cnt_i1a,
            probe9(0) => reset_in
        );

    cnt_i1a   <= std_logic_vector(to_unsigned(cnt, cnt_i1a'length));
    --********* crc signals. The crc output is 4*16b input signals(payload)
    crc_en    <= '1' when cnt < 4 else '0';
    crc_reset <= '1' when cnt = 5 else '0'; -- reset insterted at the k-char output position
    crc_in    <= LTIData_in when cnt < 4 else (others => '1');

    CRC20_inst : entity work.crc16_ltittc
        port map(
            data_in => crc_in,
            crc_en  => crc_en,
            rst     => crc_reset,
            clk     => clk240_in,
            crc_out => crc_out
        );
    busy   <= data_out_i(15);
    IsK_in <= Kchar_in;
    PROC_DECODING_DATA : process(clk240_in, reset_in)
        variable data_i : std_logic_vector(95 downto 0);
    begin
        if (reset_in = '1') then
            DETECTOR_st   <= IDLE;
            data_i        := (others => '0');
            cnt           <= 5;
            crc_valid_out <= '0';
            data_out_i    <= (others => '0');
            LTIData_in_d0 <= (others => '0');
        elsif rising_edge(clk240_in) then
            case DETECTOR_st is
                when IDLE =>
                    data_i        := (others => '0');
                    cnt           <= 0;
                    data_out_i    <= (others => '0');
                    if (LTIData_in(7 downto 0) = Kchar_comma and IsK_in(0) = '1') then
                        DETECTOR_st <= COMMA_FOUND;
                    else
                        DETECTOR_st <= IDLE;
                    end if;
                    crc_valid_out <= '0';
                when COMMA_FOUND =>
                    if (cnt /= 5) then
                        DETECTOR_st                           <= COMMA_FOUND;
                        data_i(16 * cnt + 15 downto 16 * cnt) := LTIData_in;
                        cnt                                   <= cnt + 1;
                    else
                        cnt <= 0;
                        if (LTIData_in(7 downto 0) = Kchar_comma and IsK_in(0) = '1') then
                            DETECTOR_st                           <= COMMA_FOUND;
                            data_i(16 * cnt + 15 downto 16 * cnt) := LTIData_in(15 downto 8) & x"00";
                        else            --comma not found go back IDLE
                            data_i      := (others => '0');
                            DETECTOR_st <= IDLE;
                        end if;

                        if (crc_out = LTIData_in_d0(15 downto 0)) then
                            crc_valid_out <= '1';
                        else
                            crc_valid_out <= '0';
                        end if;
                        data_out_i <= data_i; --update every 40MHz
                    end if;

                when others => DETECTOR_st <= "00";
            end case;
            LTIData_in_d0 <= LTIData_in;
        end if;
    end process;

end architecture structure;

