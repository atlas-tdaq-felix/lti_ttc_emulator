--! This file is part of the lti ttc emulator firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/lti_ttc_emulator).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Melvin Leguijt
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee, UNISIM, xpm;
use xpm.vcomponents.all;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_1164.all;

entity configuration_cdc is
    port(
        TXUSRCLK                    : in  STD_LOGIC;
        CLK_100                     : in  STD_LOGIC;
        BCR_PERIOD_in               : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
        L1A_PERIOD_in               : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
        L1A_COUNT_in                : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
        BUSY_PULSE_COUNT_in         : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
        L1A_COUNT_0_in              : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
        OCR_PERIOD_in               : in  std_logic_VECTOR(31 DOWNTO 0);
        L1A_AUTO_HALT_in            : in  STD_LOGIC;
        BUSY_EN_in                  : in  std_logic;
        LUT_CONT_EN_in              : in  std_logic;
        TTC_CONFIG_in               : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
        SYNCUSERDATA_in             : in  std_logic_VECTOR(15 DOWNTO 0);
        SORB_in                     : in  std_logic;
        ERRORFLAGS_in               : in  std_logic_VECTOR(3 DOWNTO 0);
        LBID_in                     : in  std_logic_VECTOR(15 DOWNTO 0);
        PARTITION_in                : in  std_logic_VECTOR(1 DOWNTO 0);
        SYNCGLOBALDATA_in           : in  std_logic_VECTOR(15 DOWNTO 0);
        GRST_in                     : in  std_logic;
        SYNC_in                     : in  std_logic;
        TRIGGERTYPE_in              : in  std_logic_VECTOR(15 DOWNTO 0);
        PT_in                       : in  std_logic;
        
        BCR_PERIOD_TXUSRCLK_OUT     : out STD_LOGIC_VECTOR(31 DOWNTO 0);
        L1A_PERIOD_TXUSRCLK_OUT     : out STD_LOGIC_VECTOR(31 DOWNTO 0);
        L1A_COUNT_TXUSRCLK_OUT      : out STD_LOGIC_VECTOR(31 DOWNTO 0);
        BUSY_PULSE_COUNT_100_out    : out STD_LOGIC_VECTOR(31 DOWNTO 0);
        L1A_COUNT_100_out           : out STD_LOGIC_VECTOR(31 DOWNTO 0);
        OCR_PERIOD_TXUSRCLK_OUT     : out STD_LOGIC_VECTOR(31 DOWNTO 0);
        L1A_AUTO_HALT_TXUSRCLK_OUT  : out STD_LOGIC;
        BUSY_EN_txuserclk_out       : out std_logic;
        LUT_CONT_EN_TXUSRCLK_OUT    : out std_logic;
        TTC_CONFIG_TXUSRCLK_OUT     : out STD_LOGIC_VECTOR(31 DOWNTO 0);
        SYNCUSERDATA_TXUSRCLK_OUT   : out std_logic_VECTOR(15 DOWNTO 0);
        SORB_TXUSRCLK_OUT           : out std_logic;
        ERRORFLAGS_TXUSRCLK_OUT     : out std_logic_VECTOR(3 DOWNTO 0);
        LBID_TXUSRCLK_OUT           : out std_logic_VECTOR(15 DOWNTO 0);
        PARTITION_TXUSRCLK_OUT      : out std_logic_VECTOR(1 DOWNTO 0);
        SYNCGLOBALDATA_TXUSRCLK_OUT : out std_logic_VECTOR(15 DOWNTO 0);
        GRST_TXUSRCLK_OUT           : out std_logic;
        SYNC_TXUSRCLK_OUT           : out std_logic;
        TRIGGERTYPE_TXUSRCLK_OUT    : out std_logic_VECTOR(15 DOWNTO 0);
        PT_TXUSRCLK_OUT             : out std_logic
    );
end entity configuration_cdc;

architecture RTL of configuration_cdc is

begin

    xpm_cdc_array_single_inst : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 32        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => L1A_PERIOD_in,  -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => L1A_PERIOD_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );
    xpm_cdc_array_single_TRIGGERTYPE : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 16        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => TRIGGERTYPE_in, -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => TRIGGERTYPE_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );
    xpm_cdc_array_single_PARTITION : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 2         -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => PARTITION_in,   -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => PARTITION_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );

    xpm_cdc_array_single_LBID : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 16        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => LBID_in,        -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => LBID_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
        );

    xpm_cdc_array_single_SYNCGLOBALDATA_0 : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 16        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => SYNCGLOBALDATA_in, -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => SYNCGLOBALDATA_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );

    xpm_cdc_array_single_ERRORFLAGS : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 4         -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => ERRORFLAGS_in,  -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => ERRORFLAGS_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );

    xpm_cdc_array_single_SYNCUSERDATA : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 16        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => SYNCUSERDATA_in, -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => SYNCUSERDATA_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );

    xpm_cdc_array_single_BCR_PERIOD : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 32        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => BCR_PERIOD_in,  -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => BCR_PERIOD_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );

    xpm_cdc_array_single_L1A_COUNT : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 32        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => L1A_COUNT_in,   -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => L1A_COUNT_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );

    xpm_cdc_gray_L1A_COUNT : xpm_cdc_gray
        generic map(
            DEST_SYNC_FF          => 4, -- DECIMAL; range: 2-10
            INIT_SYNC_FF          => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            REG_OUTPUT            => 0, -- DECIMAL; 0=disable registered output, 1=enable registered output
            SIM_ASSERT_CHK        => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SIM_LOSSLESS_GRAY_CHK => 0,
            WIDTH                 => 32 -- DECIMAL; range: 2-32
        )
        port map(
            src_clk      => TXUSRCLK,   -- 1-bit input: Source clock.
            src_in_bin   => L1A_COUNT_0_in, -- WIDTH-bit input: Binary input bus that will be synchronized to the
            -- destination clock domain. This output is combinatorial unless REG_OUTPUT
            -- is set to 1.

            dest_clk     => CLK_100,    -- 1-bit input: Destination clock.
            dest_out_bin => L1A_COUNT_100_out -- WIDTH-bit output: Binary input bus (src_in_bin) synchronized to
            -- destination clock domain.
        );
    xpm_cdc_gray_BUSY_PULSE_COUNT : xpm_cdc_gray
        generic map(
            DEST_SYNC_FF          => 4, -- DECIMAL; range: 2-10
            INIT_SYNC_FF          => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            REG_OUTPUT            => 0, -- DECIMAL; 0=disable registered output, 1=enable registered output
            SIM_ASSERT_CHK        => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SIM_LOSSLESS_GRAY_CHK => 0,
            WIDTH                 => 32 -- DECIMAL; range: 2-32
        )
        port map(
            src_clk      => TXUSRCLK,   -- 1-bit input: Source clock.
            src_in_bin   => BUSY_PULSE_COUNT_in, -- WIDTH-bit input: Binary input bus that will be synchronized to the
            -- destination clock domain. This output is combinatorial unless REG_OUTPUT
            -- is set to 1.

            dest_clk     => CLK_100,    -- 1-bit input: Destination clock.
            dest_out_bin => BUSY_PULSE_COUNT_100_out -- WIDTH-bit output: Binary input bus (src_in_bin) synchronized to
            -- destination clock domain.

        );

    xpm_cdc_single_inst : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => L1A_AUTO_HALT_in, -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => L1A_AUTO_HALT_TXUSRCLK_OUT -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );

    xpm_cdc_single_SORB_0 : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => SORB_in,        -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => SORB_TXUSRCLK_OUT -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );
    xpm_cdc_array_single_OCR_PERIOD : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 32        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => OCR_PERIOD_in,  -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => OCR_PERIOD_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );

    xpm_cdc_single_BUSY_EN : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => BUSY_EN_in,     -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => BUSY_EN_txuserclk_out -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );

    xpm_cdc_single_LUT_CONT_EN_0 : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => LUT_CONT_EN_in, -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => LUT_CONT_EN_TXUSRCLK_OUT -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );

    xpm_cdc_single_GRST_0 : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => GRST_in,        -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => GRST_TXUSRCLK_OUT -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );
    xpm_cdc_single_SYNC_0 : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => SYNC_in,        -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => SYNC_TXUSRCLK_OUT -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );
    xpm_cdc_single_PT_0 : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => PT_in,          -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => PT_TXUSRCLK_OUT -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );

    xpm_cdc_array_single_TTC_CONFIG : xpm_cdc_array_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,        -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 32        -- DECIMAL; range: 1-1024
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => TTC_CONFIG_in,  -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
            -- output is registered.

            dest_clk => TXUSRCLK,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => TTC_CONFIG_TXUSRCLK_OUT -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
            -- domain. It is assumed that each bit of the array is unrelated to the others.
            -- This is reflected in the constraints applied to this macro. To transfer a binary
            -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
            -- instead.

        );

end architecture RTL;
