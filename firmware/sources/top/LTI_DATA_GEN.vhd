--! This file is part of the lti ttc emulator firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/lti_ttc_emulator).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!               Melvin Leguijt
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;
use work.lti_package.all;

entity LTI_DATA_GEN is
    port(
        clk                 : in  STD_LOGIC;
        ECR_in              : in  STD_LOGIC;
        trigger_in          : in  STD_LOGIC;
        BCR_in              : in  STD_LOGIC;
        BCR_PERIOD_COUNT_IN : in  STD_LOGIC_VECTOR(11 DOWNTO 0);
        L1A_COUNT_IN        : in  STD_LOGIC_VECTOR(37 DOWNTO 0);
        ORBITID_COUNT_IN    : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
        LBID_in             : in  STD_LOGIC_VECTOR(15 downto 0);
        GRST_in             : in  STD_LOGIC;
        SL0ID_in            : in  STD_LOGIC;
        SORB_in             : in  STD_LOGIC;
        SYNC_in             : in  STD_LOGIC;
        TRIGGERTYPE_in      : in  STD_LOGIC_VECTOR(15 downto 0);
        ERRORFLAGS_in       : in  STD_LOGIC_VECTOR(3 downto 0);
        TS_in               : in  STD_LOGIC;
        SYNCGLOBALDATA_in   : in  STD_LOGIC_VECTOR(15 downto 0);
        SYNCUSERDATA_in     : in  STD_LOGIC_VECTOR(15 downto 0);
        PARTITION_in        : in  STD_LOGIC_VECTOR(1 downto 0);
        PT_in               : in  STD_LOGIC;
        
        BCR_count_en_out    : out STD_LOGIC;
        LTIData_out         : out STD_LOGIC_VECTOR(31 downto 0);
        Kchar_out           : out STD_LOGIC_VECTOR(3 downto 0);
        L0A_out             : out std_logic
    );
end entity LTI_DATA_GEN;

architecture structure of LTI_DATA_GEN is

    signal lti_data_kchar : lti_data_kchar_reg_array := lti_data_kchar_init;

    signal i_cnt : integer range 0 to 5;

    -- TTC data type
    --Byte0
    signal PT           : std_logic                     := '0';
    signal PARTITION    : std_logic_vector(1 downto 0)  := "00";
    signal BCID         : std_logic_vector(11 downto 0) := (OTHERS => '0');
    signal SYNCUSERDATA : std_logic_vector(15 downto 0) := (OTHERS => '0');

    --Byte 1
    signal SYNCGLOBALDATA : std_logic_vector(15 downto 0) := (OTHERS => '0');
    signal TS             : std_logic                     := '0';
    signal ERRORFLAGS     : std_logic_vector(3 downto 0)  := (OTHERS => '0');
    signal SL0ID          : std_logic                     := '0'; -- set L0ID
    signal SORB           : std_logic                     := '0'; -- set ORBITID
    signal SYNC           : std_logic                     := '0';
    signal GRST           : std_logic                     := '0';
    signal L0A            : std_logic                     := '0';
    signal L0ID           : std_logic_vector(37 downto 0) := (OTHERS => '0');

    --Byte 3
    signal ORBITID     : std_logic_vector(31 downto 0) := (OTHERS => '0');
    --Byte 4  
    signal TRIGGERTYPE : std_logic_vector(15 downto 0) := (OTHERS => '0');
    signal LBID        : std_logic_vector(15 downto 0) := (OTHERS => '0');
    --Byte 5
    --CRC
    signal TS_d        : std_logic                     := '0';
    signal SL0ID_d     : std_logic                     := '0'; -- set L0ID
    signal SORB_d      : std_logic                     := '0'; -- set ORBITID
    signal SYNC_d      : std_logic                     := '0';
    signal GRST_d      : std_logic                     := '0';
    signal TS_r        : std_logic                     := '0'; -- @suppress "signal TS_r is never read"
    signal SL0ID_r     : std_logic                     := '0'; -- set L0ID -- @suppress "signal SL0ID_r is never read"
    signal SORB_r      : std_logic                     := '0'; -- set ORBITID
    signal SYNC_r      : std_logic                     := '0';
    signal GRST_r      : std_logic                     := '0';

    signal CRC     : std_logic_vector(15 downto 0) := (OTHERS => '0');
    constant D16p2 : std_logic_vector(7 DOWNTO 0)  := X"50";
    constant K28p5 : std_logic_vector(7 DOWNTO 0)  := X"BC";

    signal CLK40M_en      : std_logic                     := '0';
    signal crc_en         : std_logic                     := '0';
    signal crc_reset      : std_logic                     := '0';
    signal crc_in         : std_logic_vector(31 downto 0);
    signal LTIData_out_t  : std_logic_vector(31 downto 0);
    signal LTIData_out_d  : std_logic_vector(31 downto 0);
    signal Kchar_out_d    : std_logic_vector(3 downto 0);
    -- End TTC data
    signal TTC_En         : std_logic                     := '0';
    signal reserved       : std_logic_vector(42 downto 0) := "01010101010" & X"AAAAAAAA"; -- @suppress "signal reserved is never written"
    signal AsynchUserData : std_logic_vector(63 downto 0) := X"AAAAAAAAAAAAAAAA"; -- @suppress "signal AsynchUserData is never written"

    component crc16_lti
        port(data_in          : in  std_logic_vector(31 downto 0);
             crc_en, rst, clk : in  std_logic;
             crc_out          : out std_logic_vector(15 downto 0));
    end component crc16_lti;

    component ila_lti_gen               -- @suppress "Component declaration 'ila_lti_gen' has none or multiple matching entity declarations"
        PORT(
            clk    : IN STD_LOGIC;
            probe0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            probe1 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    end component;

begin

    L0A_out <= L0A;
    
    comp_ila_lti : ila_lti_gen
        port map(
            clk       => clk,
            probe0    => LTIData_out_d,
            probe1    => Kchar_out_d,
            probe2(0) => trigger_in
        );

    comp_0 : crc16_lti
        port map(
            data_in => crc_in,
            crc_en  => crc_en,
            rst     => crc_reset,
            clk     => clk,
            crc_out => CRC);
    LTIData_out <= LTIData_out_d;
    Kchar_out   <= Kchar_out_d;
    crc_reset   <= not crc_en;
    crc_in      <= LTIData_out_t;

    process(clk)
    begin
        if (rising_edge(clk)) then
            TS_d    <= TS_in;
            SL0ID_d <= SL0ID_in;
            SORB_d  <= SORB_in;
            SYNC_d  <= SYNC_in;
            GRST_d  <= GRST_in;
            if (TS_d = '0' and TS_in = '1') then
                TS_r <= '1';
            elsif (CLK40M_en = '1') then
                TS_r <= '0';
            end if;
            if (SL0ID_d = '0' and SL0ID_in = '1') then
                SL0ID_r <= '1';
            elsif (CLK40M_en = '1') then
                SL0ID_r <= '0';
            end if;
            if (SORB_d = '0' and SORB_in = '1') then
                SORB_r <= '1';
            elsif (CLK40M_en = '1') then
                SORB_r <= '0';
            end if;
            if (SYNC_d = '0' and SYNC_in = '1') then
                SYNC_r <= '1';
            elsif (CLK40M_en = '1') then
                SYNC_r <= '0';
            end if;
            if (GRST_d = '0' and GRST_in = '1') then
                GRST_r <= '1';
            elsif (CLK40M_en = '1') then
                GRST_r <= '0';
            end if;

        end if;
    end process;

    TTC_En <= ECR_in or GRST_r or SORB_r or SYNC_r or BCR_in or trigger_in;

    process(clk)
    begin
        if (rising_edge(clk)) then
            -- software write registers
            LBID           <= LBID_in;   -- soft
            GRST           <= GRST_r;   -- soft rising
            SL0ID          <= ECR_in;    -- ECR rising
            SORB           <= SORB_r;   -- OCR rising
            SYNC           <= SYNC_r;   -- sftware rising
            TRIGGERTYPE    <= TRIGGERTYPE_in; -- soft
            ERRORFLAGS     <= ERRORFLAGS_in;  -- soft
            TS             <= BCR_in;    -- BCR 
            SYNCGLOBALDATA <= SYNCGLOBALDATA_in; -- soft
            SYNCUSERDATA   <= SYNCUSERDATA_in; -- soft
            PARTITION      <= PARTITION_in; -- soft
            PT             <= PT_in;     -- soft
            -- end soft write
            L0A            <= trigger_in;
            BCID           <= BCR_PERIOD_COUNT_IN;
            ORBITID        <= ORBITID_COUNT_IN;
            L0ID           <= L1A_COUNT_IN;

            if (CLK40M_en = '1') then
                if (TTC_En = '1') then
                    lti_data_kchar(0)(31 downto 0)  <= '0' & PT & PARTITION & BCID & SYNCUSERDATA;
                    lti_data_kchar(0)(35 downto 32) <= "0000";

                    lti_data_kchar(1)(31 downto 0)  <= SYNCGLOBALDATA & TS & ERRORFLAGS & SL0ID & SORB & SYNC & GRST & L0A & L0ID(5 downto 0);
                    lti_data_kchar(1)(35 downto 32) <= "0000";

                    lti_data_kchar(2)(31 downto 0)  <= L0ID(37 downto 6);
                    lti_data_kchar(2)(35 downto 32) <= "0000";

                    lti_data_kchar(3)(31 downto 0)  <= ORBITID;
                    lti_data_kchar(3)(35 downto 32) <= "0000";

                    lti_data_kchar(4)(31 downto 0)  <= TRIGGERTYPE & LBID;
                    lti_data_kchar(4)(35 downto 32) <= "0000";

                    lti_data_kchar(5)(31 downto 0)  <= CRC & D16p2 & K28p5;
                    lti_data_kchar(5)(35 downto 32) <= "0001";
                else
                    lti_data_kchar(0)(31 downto 0)  <= '1' & PT & PARTITION & BCID & SYNCUSERDATA;
                    lti_data_kchar(0)(35 downto 32) <= "0000";

                    lti_data_kchar(1)(31 downto 0)  <= SYNCGLOBALDATA & TS & ERRORFLAGS & reserved(10 downto 0);
                    lti_data_kchar(1)(35 downto 32) <= "0000";

                    lti_data_kchar(2)(31 downto 0)  <= AsynchUserData(31 downto 0);
                    lti_data_kchar(2)(35 downto 32) <= "0000";

                    lti_data_kchar(3)(31 downto 0)  <= AsynchUserData(63 downto 32);
                    lti_data_kchar(3)(35 downto 32) <= "0000";

                    lti_data_kchar(4)(31 downto 0)  <= reserved(41 downto 11) & '0';
                    lti_data_kchar(4)(35 downto 32) <= "0000";

                    lti_data_kchar(5)(31 downto 0)  <= CRC & D16p2 & K28p5;
                    lti_data_kchar(5)(35 downto 32) <= "0001";
                end if;
            end if;

        end if;
    end process;

    process(clk)
    begin
        if (rising_edge(clk)) then
            LTIData_out_t <= lti_data_kchar(i_cnt)(31 downto 0);
            if (crc_en = '0') then
                LTIData_out_d <= CRC & D16p2 & K28p5;
                Kchar_out_d   <= "0001";
            else
                LTIData_out_d <= LTIData_out_t;
                Kchar_out_d   <= "0000";
            end if;

            if (i_cnt < 5) then
                crc_en <= '1';
                i_cnt  <= i_cnt + 1;
            else
                crc_en <= '0';
                i_cnt  <= 0;
            end if;
        end if;
    end process;
    BCR_count_en_out <= '1' when (i_cnt = 5) else '0';
    CLK40M_en    <= '1' when (i_cnt = 5) else '0';

end architecture structure;

