--! This file is part of the lti ttc emulator firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/lti_ttc_emulator).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!               Melvin Leguijt
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee, UNISIM, xpm;
use xpm.vcomponents.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_1164.all;

entity edge_det is
    port(
        clk_160             : in  STD_LOGIC;
        sys_rst_n           : in  STD_LOGIC;

        ECR_push_in         : in  STD_LOGIC;
        trigger_push_in     : in  STD_LOGIC;
        busy_in             : in  STD_LOGIC;
        SORB_in             : in  STD_LOGIC;
        BCR_count_en_in     : in  STD_LOGIC;
        BCR_PERIOD_IN       : in  STD_LOGIC_VECTOR(31 downto 0);
        L1A_COUNT_IN        : in  STD_LOGIC_VECTOR(31 downto 0);
        L1A_PERIOD_IN       : in  STD_LOGIC_VECTOR(31 downto 0);
        TTC_CONFIG_IN       : in  STD_LOGIC_VECTOR(31 downto 0);
        LUT_CONT_EN_in      : in  STD_LOGIC;
        L1A_AUTO_HALT_in    : in  STD_LOGIC;
        BUSY_EN_in          : in  std_logic;
        doutb_in            : in  STD_LOGIC_VECTOR(31 DOWNTO 0);

        ECR_out                : out STD_LOGIC;
        trigger_out            : out STD_LOGIC;
        BCR_out                : out STD_LOGIC;
        L1A_COUNT_read_out     : out STD_LOGIC_VECTOR(31 downto 0);
        L1A_AUTO_READY_out     : out STD_LOGIC;
        BUSY_PULSE_COUNT_out   : out STD_LOGIC_VECTOR(31 downto 0);
        enb_out                : out STD_LOGIC;
        web_out                : out STD_LOGIC_VECTOR(3 DOWNTO 0);
        addrb_out              : out STD_LOGIC_VECTOR(31 DOWNTO 0);
        BCR_PERIOD_COUNT_OUT   : out STD_LOGIC_VECTOR(11 DOWNTO 0);
        L1A_COUNT_OUT          : out STD_LOGIC_VECTOR(37 DOWNTO 0);
        ORBITID_COUNT_OUT      : out STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
end entity edge_det;

architecture structure of edge_det is

    signal trigger_in : std_logic := '0';
    signal ECR_in     : std_logic := '0';
    signal BCR_in     : std_logic := '0';

    signal trigger_in_d : std_logic := '0';
    signal ECR_in_d     : std_logic := '0';
    signal BCR_in_d     : std_logic := '0';

    signal SORB_0_d : std_logic := '0';
    signal SORB_0_r : std_logic := '0';

    signal trigger_auto        : std_logic                     := '0';
    signal BCR_auto            : std_logic                     := '0';
    signal TTC_CONFIG_IN_L1A_d : std_logic_vector(1 downto 0)  := "00";
    signal L1A_PERIOD_COUNT    : std_logic_vector(31 downto 0) := (others => '0');
    signal L1A_TRIGGER_COUNT   : std_logic_vector(31 downto 0) := (others => '0');
    signal BCR_PERIOD_COUNT    : std_logic_vector(31 downto 0) := (others => '0');
    signal L1A_LUT_COUNT       : std_logic_vector(31 downto 0) := (others => '0');
    signal L1A_AUTO_HALT_d     : std_logic                     := '0';
    signal L1A_COUNT_read_d    : std_logic_vector(37 downto 0);
    signal trigger_lut         : std_logic                     := '0';
    signal enb_t               : STD_LOGIC;
    signal enb_t_d             : STD_LOGIC;
    signal enb_t_dd            : STD_LOGIC;
    signal addrb_t             : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal doutb_t             : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal CLK40M_en           : std_logic                     := '0';
    signal busy_all            : std_logic                     := '0';
    signal BUSY_PULSE_COUNT_t  : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal trigger_in_xpm      : STD_LOGIC                     := '0';
    signal ECR_in_xpm          : STD_LOGIC                     := '0';
    signal ORBITID_COUNT       : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal ECR_r_d             : std_logic                     := '0';

    component ila_rd_8t49n242           -- @suppress "Unused declaration" -- @suppress "Component declaration 'ila_rd_8t49n242' has none or multiple matching entity declarations"
        PORT(
            clk    : IN STD_LOGIC;
            probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    end component;

begin

    xpm_cdc_single_trigger : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 2,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => trigger_push_in,   -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => clk_160,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => trigger_in_xpm  -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );

    xpm_cdc_single_ecr : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 2,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => ECR_push_in,       -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            dest_clk => clk_160,       -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => ECR_in_xpm      -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );

    trigger_in <= (trigger_auto or trigger_lut) when TTC_CONFIG_IN_L1A_d /= "00" else trigger_in_xpm;
    ECR_in     <= ECR_in_xpm;
    BCR_in     <= BCR_auto;

    enb_out   <= enb_t;
    addrb_out <= addrb_t(29 downto 0) & "00";
    web_out   <= (others => '0');

    CLK40M_en <= BCR_count_en_in;

    L1A_COUNT_read_out   <= L1A_COUNT_read_d(31 downto 0);
    L1A_AUTO_HALT_d  <= L1A_AUTO_HALT_in;
    busy_all         <= BUSY_EN_in and (not busy_in);
    BUSY_PULSE_COUNT_out <= BUSY_PULSE_COUNT_t;

    -- LTI interface
    L1A_COUNT_OUT        <= L1A_COUNT_read_d;
    BCR_PERIOD_COUNT_OUT <= BCR_PERIOD_COUNT(11 downto 0);
    ORBITID_COUNT_OUT    <= ORBITID_COUNT;
    process(clk_160, sys_rst_n)
    begin
        if (sys_rst_n = '0') then
            BCR_in_d            <= '0';
            ECR_in_d            <= '0';
            trigger_in_d        <= '0';
            trigger_auto        <= '0';
            TTC_CONFIG_IN_L1A_d <= "00";
            enb_t               <= '1';
            enb_t_d             <= '0';
            enb_t_dd            <= '0';
            addrb_t             <= (others => '0');
            doutb_t             <= (others => '0');
            L1A_AUTO_READY_out      <= '0';
            BUSY_PULSE_COUNT_t  <= (others => '0');
            BCR_PERIOD_COUNT    <= (others => '0');
            BCR_auto            <= '0';
            ORBITID_COUNT       <= (others => '0');
            BCR_out             <= '0';
            ECR_out             <= '0';
            ECR_r_d             <= '0';
            L1A_COUNT_read_d    <= (others => '0');
            L1A_LUT_COUNT       <= (others => '0');
            L1A_PERIOD_COUNT    <= (others => '0');
            L1A_TRIGGER_COUNT   <= (others => '0');
            trigger_lut         <= '0';
            trigger_out         <= '0';
            SORB_0_d            <= '0';
            SORB_0_r            <= '0';

        elsif (rising_edge(clk_160)) then

            BCR_in_d <= BCR_in;
            if (BCR_in = '1' and BCR_in_d = '0') then
                BCR_out <= '1';
            elsif (CLK40M_en = '1') then
                BCR_out <= '0';
            end if;

            ECR_in_d <= ECR_in;
            if (ECR_in = '1' and ECR_in_d = '0') then
                ECR_out   <= '1';
                ECR_r_d <= '1';
            elsif (CLK40M_en = '1') then
                ECR_out   <= '0';
                ECR_r_d <= '0';
            end if;

            trigger_in_d <= trigger_in;
            if (trigger_in = '1' and trigger_in_d = '0') then
                trigger_out <= '1';
            elsif (CLK40M_en = '1') then
                trigger_out <= '0';
            end if;

            SORB_0_d <= SORB_in;
            if (SORB_in = '1' and SORB_0_d = '0') then
                SORB_0_r <= '1';
            elsif (CLK40M_en = '1') then
                SORB_0_r <= '0';
            end if;

            TTC_CONFIG_IN_L1A_d <= TTC_CONFIG_IN(1 downto 0);
            enb_t_d             <= enb_t;
            enb_t_dd            <= enb_t_d;
            case TTC_CONFIG_IN_L1A_d is
                when "00" =>
                    L1A_LUT_COUNT     <= (others => '0');
                    L1A_TRIGGER_COUNT <= (others => '0');
                    L1A_PERIOD_COUNT  <= (others => '0');
                    trigger_auto      <= '0';
                    trigger_lut       <= '0';
                    addrb_t           <= (others => '0');
                    enb_t             <= '1';
                    doutb_t           <= doutb_in;
                    L1A_AUTO_READY_out    <= '0';
                when "01" =>
                    trigger_lut <= '0';
                    if (CLK40M_en = '1' and L1A_AUTO_HALT_d = '0' and busy_all = '0') then
                        if (L1A_COUNT_IN = X"00000000") then
                            if (L1A_PERIOD_IN = X"00000000") then
                                L1A_PERIOD_COUNT <= (others => '0');
                                trigger_auto     <= '0';
                            else
                                if (L1A_PERIOD_COUNT < L1A_PERIOD_IN) then
                                    L1A_PERIOD_COUNT <= L1A_PERIOD_COUNT + 1;
                                    trigger_auto     <= '0';
                                else
                                    L1A_PERIOD_COUNT <= (others => '0');
                                    trigger_auto     <= '1';
                                end if;
                            end if;
                        else
                            if (L1A_PERIOD_IN = X"00000000") then
                                L1A_PERIOD_COUNT  <= (others => '0');
                                L1A_TRIGGER_COUNT <= (others => '0');
                                trigger_auto      <= '0';
                            else
                                if (L1A_PERIOD_COUNT < L1A_PERIOD_IN - 1) then
                                    L1A_PERIOD_COUNT <= L1A_PERIOD_COUNT + 1;
                                    trigger_auto     <= '0';
                                else
                                    if (L1A_TRIGGER_COUNT < L1A_COUNT_IN) then
                                        L1A_TRIGGER_COUNT <= L1A_TRIGGER_COUNT + 1;
                                        L1A_PERIOD_COUNT  <= (others => '0');
                                        trigger_auto      <= '1';
                                    else
                                        L1A_TRIGGER_COUNT <= L1A_TRIGGER_COUNT;
                                        L1A_PERIOD_COUNT  <= L1A_PERIOD_COUNT;
                                        L1A_AUTO_READY_out    <= '1';
                                        trigger_auto      <= '0';
                                    end if;
                                end if;
                            end if;
                        end if;
                    else
                        L1A_TRIGGER_COUNT <= L1A_TRIGGER_COUNT;
                        L1A_PERIOD_COUNT  <= L1A_PERIOD_COUNT;
                        trigger_auto      <= '0';
                    end if;
                when "10" =>
                    trigger_auto <= '0';
                    if (CLK40M_en = '1' and L1A_AUTO_HALT_d = '0' and busy_all = '0') then
                        if (L1A_COUNT_IN = X"00000000") then
                            L1A_LUT_COUNT <= (others => '0');
                            addrb_t       <= (others => '0');
                            enb_t         <= '0';
                            trigger_lut   <= '0';
                        else
                            if (doutb_t = X"00000000") then
                                if (addrb_t < L1A_COUNT_IN) then
                                    L1A_LUT_COUNT <= (others => '0');
                                    addrb_t       <= addrb_t + 1;
                                    enb_t         <= '1';
                                elsif (LUT_CONT_EN_in = '1') then
                                    L1A_LUT_COUNT <= (others => '0');
                                    addrb_t       <= (others => '0');
                                    enb_t         <= '1';
                                else
                                    L1A_LUT_COUNT <= L1A_LUT_COUNT;
                                    addrb_t       <= addrb_t;
                                    enb_t         <= '0';
                                end if;
                            elsif (L1A_LUT_COUNT < doutb_t - 1) then
                                L1A_LUT_COUNT <= L1A_LUT_COUNT + 1;
                                trigger_lut   <= '0';
                                enb_t         <= '0';
                            else
                                if (addrb_t < L1A_COUNT_IN) then
                                    L1A_LUT_COUNT <= (others => '0');
                                    addrb_t       <= addrb_t + 1;
                                    enb_t         <= '1';
                                    trigger_lut   <= '1';
                                else
                                    if (LUT_CONT_EN_in = '0') then
                                        L1A_LUT_COUNT  <= L1A_LUT_COUNT;
                                        addrb_t        <= addrb_t;
                                        enb_t          <= '0';
                                        trigger_lut    <= '0';
                                        L1A_AUTO_READY_out <= '1';
                                    else
                                        L1A_LUT_COUNT <= (others => '0');
                                        addrb_t       <= (others => '0');
                                        enb_t         <= '1';
                                        trigger_lut   <= '1';
                                    end if;
                                end if;
                            end if;
                        end if;
                    else
                        L1A_LUT_COUNT <= L1A_LUT_COUNT;
                        addrb_t       <= addrb_t;
                        trigger_lut   <= '0';
                        enb_t         <= '0';
                        if (enb_t_dd = '1') then
                            doutb_t <= doutb_in;
                        else
                            doutb_t <= doutb_t;
                        end if;
                    end if;
                when others =>
                    L1A_LUT_COUNT     <= (others => '0');
                    L1A_PERIOD_COUNT  <= (others => '0');
                    trigger_lut       <= '0';
                    trigger_auto      <= '0';
                    addrb_t           <= (others => '0');
                    enb_t             <= '0';
                    L1A_TRIGGER_COUNT <= (others => '0');
            end case;

            if (CLK40M_en = '1') then
                if (BCR_PERIOD_IN = X"00000000") then
                    BCR_PERIOD_COUNT <= (others => '0');
                    BCR_auto         <= '0';
                    ORBITID_COUNT    <= ORBITID_COUNT;
                else
                    if (BCR_PERIOD_COUNT < BCR_PERIOD_IN - 1) then
                        BCR_PERIOD_COUNT <= BCR_PERIOD_COUNT + 1;
                        BCR_auto         <= '0';
                        ORBITID_COUNT    <= ORBITID_COUNT;
                    else
                        BCR_PERIOD_COUNT <= (others => '0');
                        BCR_auto         <= '1';
                        if (SORB_0_r = '0') then
                            ORBITID_COUNT <= ORBITID_COUNT + 1;
                        else
                            ORBITID_COUNT <= (others => '0');
                        end if;
                    end if;
                end if;
            else
                BCR_PERIOD_COUNT <= BCR_PERIOD_COUNT;
                ORBITID_COUNT    <= ORBITID_COUNT;
                BCR_auto         <= '0';
            end if;

            if (ECR_r_d = '1') then
                L1A_COUNT_read_d <= (others => '0');
            elsif (TTC_CONFIG_IN_L1A_d /= "00") then
                if (trigger_auto = '1' or trigger_lut = '1') then
                    L1A_COUNT_read_d <= L1A_COUNT_read_d + 1;
                else
                    L1A_COUNT_read_d <= L1A_COUNT_read_d;
                end if;
            end if;

            if (busy_all = '1' and CLK40M_en = '1') then
                BUSY_PULSE_COUNT_t <= BUSY_PULSE_COUNT_t + 1;
            else
                BUSY_PULSE_COUNT_t <= BUSY_PULSE_COUNT_t;
            end if;

        end if;
    end process;

end architecture structure;
