--! This file is part of the lti ttc emulator firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/lti_ttc_emulator).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!               Melvin Leguijt
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;
use work.lti_package.all;
library unisim;
use unisim.vcomponents.all;
Library xpm;
use xpm.vcomponents.all;

entity TTC_ZYNQUltraScale_WRAPPER is
    port(
        CLK_100_CAL               : in  std_logic;
        gtxtxn_out                : out std_logic_vector(4 downto 0);
        gtxtxp_out                : out std_logic_vector(4 downto 0);
        gtxrxp_in                 : in  std_logic_vector(4 downto 0);
        gtxrxn_in                 : in  std_logic_vector(4 downto 0);
        rxdata_out                : out array_std_logic_vector16_type;
        Kchar_rx                  : out array_std_logic_vector2_type;
        txdata_in                 : in  array_std_logic_vector32_type;
        Kchar_tx                  : in  array_std_logic_vector4_type;
        RXUSRCLK_OUT              : out std_logic_vector(4 downto 0);
        TXUSRCLK_OUT              : out std_logic;
        TX_DISABLE                : in  std_logic_vector(4 downto 0);
        reset_in                  : in  std_logic_vector(2 downto 0);
        Q0_CLK0_GTREFCLK_PAD_P    : in  std_logic;
        Q0_CLK0_GTREFCLK_PAD_N    : in  std_logic;
        Q0_CLK1_GTREFCLK_PAD_P    : in  std_logic;
        Q0_CLK1_GTREFCLK_PAD_N    : in  std_logic;
        Q0_CLK0_GTREFCLK_PAD_div2 : out std_logic;
        CLK_40_out                : out std_logic;
        GTH_refclk_sel_in         : in std_logic 
    );
end entity TTC_ZYNQUltraScale_WRAPPER;

--------------------------------------------------------------------------------
-- Object        : Architecture xil_defaultlib.TTC_GTX2ZYNQ7000_WRAPPER.structure
-- Last modified : Thu Mar  5 16:45:18 2020
--------------------------------------------------------------------------------

architecture structure of TTC_ZYNQUltraScale_WRAPPER is
    signal txctrl0_in             : array_std_logic_vector16_type; -- @suppress "signal txctrl0_in is never read"
    signal txctrl1_in             : array_std_logic_vector16_type; -- @suppress "signal txctrl1_in is never read"
    signal txctrl2_in             : array_std_logic_vector8_type;
    signal txdata_in_t            : array_std_logic_vector32_type;
    signal rxctrl0_out            : array_std_logic_vector16_type;
    signal rxctrl1_out            : array_std_logic_vector16_type; -- @suppress "signal rxctrl1_out is never read"
    signal rxctrl2_out            : array_std_logic_vector8_type; -- @suppress "signal rxctrl2_out is never read"
    signal rxctrl3_out            : array_std_logic_vector8_type; -- @suppress "signal rxctrl3_out is never read"
    signal rxdata_out_t           : array_std_logic_vector16_type;
    signal RXUSRCLK_BUF           : std_logic_vector(4 downto 0);
    signal Q0_CLK0_GTREFCLK_PAD_0 : std_logic;
    signal Q0_CLK0_GTREFCLK_PAD_1 : std_logic;
    signal txoutclk               : std_logic_vector(4 downto 0);
    signal txusrclk               : std_logic_vector(4 downto 0);

    signal Q0_CLK0_GTREFCLK_PAD_div2_pre : std_logic;

    signal qpll1refclksel   : std_logic_vector(2 downto 0);
    signal qpll1refclksel_2 : std_logic_vector(2 downto 0);

    COMPONENT clk_sel -- @suppress "Component declaration 'clk_sel' has none or multiple matching entity declarations"
        PORT(
            clk        : IN  STD_LOGIC;
            probe_out0 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
        );
    END COMPONENT;

    COMPONENT gtwizard_ultrascale_0 -- @suppress "Component declaration 'gtwizard_ultrascale_0' has none or multiple matching entity declarations"
        PORT(
            gtwiz_userclk_tx_reset_in          : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_srcclk_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_usrclk_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_usrclk2_out       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_active_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_reset_in          : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_srcclk_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_usrclk_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_usrclk2_out       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_active_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in                 : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in               : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
            gtwiz_userdata_rx_out              : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            gtrefclk01_in                      : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtrefclk11_in                      : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtsouthrefclk01_in                 : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1refclksel_in                  : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
            qpll1outclk_out                    : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outrefclk_out                 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxn_in                          : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxp_in                          : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            rx8b10ben_in                       : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcommadeten_in                    : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxmcommaalignen_in                 : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpcommaalignen_in                 : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            tx8b10ben_in                       : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            txctrl0_in                         : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
            txctrl1_in                         : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
            txctrl2_in                         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
            gthtxn_out                         : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthtxp_out                         : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtpowergood_out                    : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxbyteisaligned_out                : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxbyterealign_out                  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcommadet_out                     : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxctrl0_out                        : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            rxctrl1_out                        : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            rxctrl2_out                        : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            rxctrl3_out                        : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            rxoutclk_out                       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txoutclk_out                       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

    COMPONENT gtwizard_ultrascale_1 -- @suppress "Component declaration 'gtwizard_ultrascale_1' has none or multiple matching entity declarations"
        PORT(
            gtwiz_userclk_tx_reset_in          : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_srcclk_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_usrclk_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_usrclk2_out       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_active_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_reset_in          : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_srcclk_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_usrclk_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_usrclk2_out       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_active_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in                 : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in         : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out            : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in               : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
            gtwiz_userdata_rx_out              : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
            gtnorthrefclk11_in                 : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtrefclk01_in                      : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1refclksel_in                  : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
            qpll1outclk_out                    : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outrefclk_out                 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxn_in                          : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
            gthrxp_in                          : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
            rx8b10ben_in                       : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxcommadeten_in                    : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxmcommaalignen_in                 : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxpcommaalignen_in                 : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
            tx8b10ben_in                       : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
            txctrl0_in                         : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
            txctrl1_in                         : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
            txctrl2_in                         : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
            gthtxn_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gthtxp_out                         : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtpowergood_out                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxbyteisaligned_out                : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxbyterealign_out                  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxcommadet_out                     : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxctrl0_out                        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
            rxctrl1_out                        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
            rxctrl2_out                        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            rxctrl3_out                        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            rxoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txoutclk_out                       : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txpmaresetdone_out                 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
        );
    END COMPONENT;

    signal rx_reset_done_out   : std_logic_vector(4 downto 0); -- @suppress "signal rx_reset_done_out is never read"
    signal tx_reset_done_out   : std_logic_vector(4 downto 0); -- @suppress "signal tx_reset_done_out is never read"
    signal rxbyteisaligned_out : std_logic_vector(4 downto 0); -- @suppress "signal rxbyteisaligned_out is never read"
    signal rxbyterealign_out   : std_logic_vector(4 downto 0); -- @suppress "signal rxbyterealign_out is never read"
    signal rxcommadet_out      : std_logic_vector(4 downto 0); -- @suppress "signal rxcommadet_out is never read"
    signal rxpmaresetdone_out  : std_logic_vector(4 downto 0); -- @suppress "signal rxpmaresetdone_out is never read"
    signal txpmaresetdone_out  : std_logic_vector(4 downto 0); -- @suppress "signal txpmaresetdone_out is never read"

    signal rxoutclk_out_s       : std_logic_vector(4 downto 0);
    signal TX_DISABLE_100       : std_logic_vector(4 downto 0);
    signal userdata_rx_out_quad : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal rxctrl0_out_quad     : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal rxctrl1_out_quad     : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal rxctrl2_out_quad     : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal rxctrl3_out_quad     : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal userdata_tx_in_quad  : STD_LOGIC_VECTOR(127 DOWNTO 0);
    signal txctrl0_out_quad     : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal txctrl1_out_quad     : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal txctrl2_out_quad     : STD_LOGIC_VECTOR(31 DOWNTO 0);
    --

begin
    --
    gth_inst : for i in 0 to 4 generate
        QPLL_CLKSEL : process(CLK_100_CAL)
        begin
            if rising_edge(CLK_100_CAL ) then
                if GTH_refclk_sel_in = '1' then -- FMC Refclk
                    qpll1refclksel   <= "101";
                    qpll1refclksel_2 <= "001";
                else                    -- ST1 baseboard Refclk
                    qpll1refclksel   <= "010";
                    qpll1refclksel_2 <= "100";
                end if;
            end if;
        end process QPLL_CLKSEL;

        --Tx data interface & synchronization
        txctrl1_in(i) <= X"0000";
        txctrl0_in(i) <= X"0000";

        cdc_TX_DISABLE : xpm_cdc_single
            generic map(
                DEST_SYNC_FF   => 4,    -- DECIMAL; range: 2-10
                INIT_SYNC_FF   => 0,    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                SIM_ASSERT_CHK => 0,    -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                SRC_INPUT_REG  => 0     -- DECIMAL; 0=do not register input, 1=register input
            )
            port map(
                src_clk  => '0',        -- 1-bit input: optional; required when SRC_INPUT_REG = 1
                src_in   => TX_DISABLE(i), -- 1-bit input: Input signal to be synchronized to dest_clk domain.
                -- is registered.

                dest_clk => txusrclk(0), -- 1-bit input: Clock signal for the destination clock domain.
                dest_out => TX_DISABLE_100(i) -- 1-bit output: src_in synchronized to the destination clock domain. This output
            );

        txctrl2_in(i)  <= "0000" & Kchar_tx(i) when TX_DISABLE_100(i) = '0' else "00000001";
        txdata_in_t(i) <= txdata_in(i) when TX_DISABLE_100(i) = '0' else X"000000BC";

        --Rx data interface
        rxdata_out(i)   <= rxdata_out_t(i);
        Kchar_rx(i)     <= rxctrl0_out(i)(1 downto 0);
        RXUSRCLK_OUT(i) <= RXUSRCLK_BUF(i);

    end generate;

    IBUFDS_GTE4_inst : IBUFDS_GTE4
        generic map(
            REFCLK_EN_TX_PATH  => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX    => "00"
        )
        port map(
            O     => Q0_CLK0_GTREFCLK_PAD_0, -- 1-bit output: Refer to Transceiver User Guide.
            ODIV2 => Q0_CLK0_GTREFCLK_PAD_div2_pre, -- 1-bit output: Refer to Transceiver User Guide.
            CEB   => '0',               -- 1-bit input: Refer to Transceiver User Guide.
            I     => Q0_CLK0_GTREFCLK_PAD_P, -- 1-bit input: Refer to Transceiver User Guide.
            IB    => Q0_CLK0_GTREFCLK_PAD_N -- 1-bit input: Refer to Transceiver User Guide.
        );
    IBUFDS_REFCLK_2 : IBUFDS_GTE4
        generic map(
            REFCLK_EN_TX_PATH  => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX    => "00"
        )
        port map(
            O     => Q0_CLK0_GTREFCLK_PAD_1, -- 1-bit output: Refer to Transceiver User Guide.
            ODIV2 => open,              -- 1-bit output: Refer to Transceiver User Guide.
            CEB   => '0',               -- 1-bit input: Refer to Transceiver User Guide.
            I     => Q0_CLK1_GTREFCLK_PAD_P, -- 1-bit input: Refer to Transceiver User Guide.
            IB    => Q0_CLK1_GTREFCLK_PAD_N -- 1-bit input: Refer to Transceiver User Guide.
        );

    Q0_CLK0_GTREFCLK_PAD_div2_inst : BUFG_GT
        port map(
            O       => Q0_CLK0_GTREFCLK_PAD_div2,
            CE      => '1',
            CEMASK  => '0',
            CLR     => '0',
            CLRMASK => '0',
            DIV     => "000",
            I       => Q0_CLK0_GTREFCLK_PAD_div2_pre
        );

    TXUSRCLK_OUT <= txusrclk(0);
    
    BUFGCE_DIV_inst_40 : BUFGCE_DIV
        generic map(
            BUFGCE_DIVIDE   => 6,       -- 1-8 
            -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
            IS_CE_INVERTED  => '0',     -- Optional inversion for CE
            IS_CLR_INVERTED => '0',     -- Optional inversion for CLR
            IS_I_INVERTED   => '0',     -- Optional inversion for I
            SIM_DEVICE      => "ULTRASCALE_PLUS"
        )
        port map(
            O   => CLK_40_out,
            CE  => '1',
            CLR => reset_in(0),
            I   => txusrclk(0)
        );

    bufg0_txusrclk : BUFG_GT -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
        port map(
            O       => txusrclk(0),
            CE      => '1',
            CEMASK  => '0',
            CLR     => '0',
            CLRMASK => '0',
            DIV     => "000",
            I       => txoutclk(1)
        );

    RXUSRCLK_BUF_inst : for i in 0 to 4 generate
        bufg0_rxusrclk : BUFG_GT -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
            port map(
                O       => RXUSRCLK_BUF(i),
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => rxoutclk_out_s(i)
            );
    end generate;

    g_channel_0_0 : gtwizard_ultrascale_0
        PORT MAP(
            gtwiz_userclk_tx_reset_in          => reset_in(2 downto 2),
            gtwiz_userclk_rx_reset_in          => "0",
            gtwiz_userclk_rx_srcclk_out        => open,
            gtwiz_userclk_rx_usrclk_out        => open,
            gtwiz_userclk_rx_usrclk2_out       => open,
            gtwiz_userclk_rx_active_out        => open,
            gtwiz_reset_clk_freerun_in(0)      => CLK_100_CAL,
            gtwiz_reset_all_in                 => reset_in(0 downto 0),
            gtwiz_reset_tx_pll_and_datapath_in => reset_in(1 downto 1),
            gtwiz_reset_tx_datapath_in         => reset_in(1 downto 1),
            gtwiz_reset_rx_pll_and_datapath_in => reset_in(2 downto 2),
            gtwiz_reset_rx_datapath_in         => reset_in(2 downto 2),
            gtwiz_reset_rx_cdr_stable_out      => open,
            gtwiz_reset_tx_done_out            => tx_reset_done_out(0 downto 0),
            gtwiz_reset_rx_done_out            => rx_reset_done_out(0 downto 0),
            gtwiz_userdata_tx_in               => txdata_in_t(0),
            gtwiz_userdata_rx_out(15 downto 0) => rxdata_out_t(0),
            gthrxn_in                          => gtxrxn_in(0 downto 0),
            gthrxp_in                          => gtxrxp_in(0 downto 0),
            rxcommadeten_in                    => (others => '1'),
            rxmcommaalignen_in                 => (others => '1'),
            rxpcommaalignen_in                 => (others => '1'),
            rxbyteisaligned_out                => rxbyteisaligned_out(0 downto 0),
            rxbyterealign_out                  => rxbyterealign_out(0 downto 0),
            rxcommadet_out                     => rxcommadet_out(0 downto 0),
            gthtxn_out                         => gtxtxn_out(0 downto 0),
            gthtxp_out                         => gtxtxp_out(0 downto 0),
            gtpowergood_out                    => open,
            rxpmaresetdone_out                 => rxpmaresetdone_out(0 downto 0),
            txpmaresetdone_out                 => txpmaresetdone_out(0 downto 0),
            rx8b10ben_in                       => (others => '1'),
            tx8b10ben_in                       => (others => '1'),
            txctrl0_in                         => txctrl0_in(0),
            txctrl1_in                         => txctrl1_in(0),
            txctrl2_in                         => txctrl2_in(0),
            rxctrl0_out                        => rxctrl0_out(0),
            rxctrl1_out                        => rxctrl1_out(0),
            rxctrl2_out                        => rxctrl2_out(0),
            rxctrl3_out                        => rxctrl3_out(0),
            rxoutclk_out                       => rxoutclk_out_s(0 downto 0),
            gtwiz_userclk_tx_srcclk_out        => open,
            gtwiz_userclk_tx_usrclk_out        => open,
            gtwiz_userclk_tx_usrclk2_out       => open,
            gtwiz_userclk_tx_active_out        => open,
            txoutclk_out                       => open,
            qpll1outclk_out                    => open,
            qpll1outrefclk_out                 => open,
            gtrefclk01_in(0)                   => '0',
            gtsouthrefclk01_in(0)              => Q0_CLK0_GTREFCLK_PAD_1,
            qpll1refclksel_in                  => qpll1refclksel,
            gtrefclk11_in(0)                   => Q0_CLK0_GTREFCLK_PAD_0
        );

    gt_channel_inst0 : gtwizard_ultrascale_1
        PORT MAP(
            gtwiz_userclk_tx_reset_in          => "0",
            txoutclk_out                       => txoutclk(4 downto 1),
            gtwiz_userclk_rx_reset_in          => "0",
            gtwiz_userclk_rx_srcclk_out        => open,
            gtwiz_userclk_rx_usrclk_out        => open,
            gtwiz_userclk_rx_usrclk2_out       => open,
            gtwiz_userclk_rx_active_out        => open,
            gtwiz_reset_clk_freerun_in(0)      => CLK_100_CAL,
            gtwiz_reset_all_in                 => reset_in(0 downto 0),
            gtwiz_reset_tx_pll_and_datapath_in => reset_in(1 downto 1),
            gtwiz_reset_tx_datapath_in         => reset_in(1 downto 1),
            gtwiz_reset_rx_pll_and_datapath_in => reset_in(2 downto 2),
            gtwiz_reset_rx_datapath_in         => reset_in(2 downto 2),
            gtwiz_reset_rx_cdr_stable_out      => open,
            gtwiz_reset_tx_done_out            => tx_reset_done_out(1 downto 1),
            gtwiz_reset_rx_done_out            => rx_reset_done_out(1 downto 1),
            gtwiz_userdata_tx_in               => userdata_tx_in_quad,
            gtwiz_userdata_rx_out              => userdata_rx_out_quad,
            gthrxn_in                          => gtxrxn_in(4 downto 1),
            gthrxp_in                          => gtxrxp_in(4 downto 1),
            rxcommadeten_in                    => (others => '1'),
            rxmcommaalignen_in                 => (others => '1'),
            rxpcommaalignen_in                 => (others => '1'),
            rxbyteisaligned_out                => rxbyteisaligned_out(4 downto 1),
            rxbyterealign_out                  => rxbyterealign_out(4 downto 1),
            rxcommadet_out                     => rxcommadet_out(4 downto 1),
            gthtxn_out                         => gtxtxn_out(4 downto 1),
            gthtxp_out                         => gtxtxp_out(4 downto 1),
            gtpowergood_out                    => open,
            rxpmaresetdone_out                 => rxpmaresetdone_out(4 downto 1),
            txpmaresetdone_out                 => txpmaresetdone_out(4 downto 1),
            rx8b10ben_in                       => (others => '1'),
            tx8b10ben_in                       => (others => '1'),
            txctrl0_in                         => txctrl0_out_quad,
            txctrl1_in                         => txctrl1_out_quad,
            txctrl2_in                         => txctrl2_out_quad,
            rxctrl0_out                        => rxctrl0_out_quad,
            rxctrl1_out                        => rxctrl1_out_quad,
            rxctrl2_out                        => rxctrl2_out_quad,
            rxctrl3_out                        => rxctrl3_out_quad,
            rxoutclk_out                       => rxoutclk_out_s(4 downto 1),
            --gtrefclk0_in                       => (others => Q0_CLK0_GTREFCLK_PAD),
            gtrefclk01_in(0)                   => Q0_CLK0_GTREFCLK_PAD_1,
            gtwiz_userclk_tx_srcclk_out        => open,
            gtwiz_userclk_tx_usrclk_out        => open,
            gtwiz_userclk_tx_usrclk2_out       => open,
            gtwiz_userclk_tx_active_out        => open,
            qpll1outclk_out                    => open,
            qpll1outrefclk_out                 => open,
            gtnorthrefclk11_in(0)              => Q0_CLK0_GTREFCLK_PAD_0,
            qpll1refclksel_in                  => qpll1refclksel_2
        );

    txctrl0_out_quad <= txctrl0_in(4) & txctrl0_in(3) & txctrl0_in(2) & txctrl0_in(1);
    txctrl1_out_quad <= txctrl1_in(4) & txctrl1_in(3) & txctrl1_in(2) & txctrl1_in(1);
    txctrl2_out_quad <= txctrl2_in(4) & txctrl2_in(3) & txctrl2_in(2) & txctrl2_in(1);

    userdata_tx_in_quad <= txdata_in_t(4) & txdata_in_t(3) & txdata_in_t(2) & txdata_in_t(1);

    data_rcv_inst : for i in 1 to 4 generate
        rxdata_out_t(i) <= userdata_rx_out_quad((15 * i) downto (15 * (i - 1)));

        rxctrl0_out(i) <= rxctrl0_out_quad((15 * i) downto (15 * (i - 1)));
        rxctrl1_out(i) <= rxctrl1_out_quad((15 * i) downto (15 * (i - 1)));
        rxctrl2_out(i) <= rxctrl2_out_quad((7 * i) downto (7 * (i - 1)));
        rxctrl3_out(i) <= rxctrl3_out_quad((7 * i) downto (7 * (i - 1)));

    end generate;

end architecture structure;             -- of TTC_GTX2ZYNQ7000_WRAPPER

