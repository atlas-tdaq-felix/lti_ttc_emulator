--! This file is part of the lti ttc emulator firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/lti_ttc_emulator).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!               Melvin Leguijt
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

---------------------------------------------------------------------------------------------------
-- libraries
---------------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_std_unsigned.all;
use work.lti_package.all;
Library UNISIM;
use UNISIM.vcomponents.all;
library xpm;
use xpm.VCOMPONENTS.all;

---------------------------------------------------------------------------------------------------
-- entity declaration
---------------------------------------------------------------------------------------------------
entity lti_emulator_top is
    generic (
        G_MAIN_REFCLK_SEL : std_logic := '0' -- 0:Enclustra si570, 1 FMC Refclk
    );
    port(
        -- Anios_0
        IO0_D0_P                  : inout std_logic; -- @suppress "Unused port: IO0_D0_P is not used in work.lti_emulator_top(rtl)"
        IO0_D1_N                  : inout std_logic; -- @suppress "Unused port: IO0_D1_N is not used in work.lti_emulator_top(rtl)"
        IO0_D2_P                  : inout std_logic; -- @suppress "Unused port: IO0_D2_P is not used in work.lti_emulator_top(rtl)"
        IO0_D3_N                  : inout std_logic; -- @suppress "Unused port: IO0_D3_N is not used in work.lti_emulator_top(rtl)"
        IO0_D4_P                  : inout std_logic; -- @suppress "Unused port: IO0_D4_P is not used in work.lti_emulator_top(rtl)"
        IO0_D5_N                  : inout std_logic; -- @suppress "Unused port: IO0_D5_N is not used in work.lti_emulator_top(rtl)"
        IO0_D6_P                  : inout std_logic; -- @suppress "Unused port: IO0_D6_P is not used in work.lti_emulator_top(rtl)"
        IO0_D7_N                  : inout std_logic; -- @suppress "Unused port: IO0_D7_N is not used in work.lti_emulator_top(rtl)"
        IO0_D8_P                  : inout std_logic; -- @suppress "Unused port: IO0_D8_P is not used in work.lti_emulator_top(rtl)"
        IO0_D9_N                  : inout std_logic; -- @suppress "Unused port: IO0_D9_N is not used in work.lti_emulator_top(rtl)"
        IO0_D10_P                 : inout std_logic; -- @suppress "Unused port: IO0_D10_P is not used in work.lti_emulator_top(rtl)"
        IO0_D11_N                 : inout std_logic; -- @suppress "Unused port: IO0_D11_N is not used in work.lti_emulator_top(rtl)"
        IO0_D12_P                 : inout std_logic; -- @suppress "Unused port: IO0_D12_P is not used in work.lti_emulator_top(rtl)"
        IO0_D13_N                 : inout std_logic; -- @suppress "Unused port: IO0_D13_N is not used in work.lti_emulator_top(rtl)"
        IO0_D14_P                 : inout std_logic; -- @suppress "Unused port: IO0_D14_P is not used in work.lti_emulator_top(rtl)"
        IO0_D15_N                 : inout std_logic; -- @suppress "Unused port: IO0_D15_N is not used in work.lti_emulator_top(rtl)"
        IO0_D16_P                 : inout std_logic; -- @suppress "Unused port: IO0_D16_P is not used in work.lti_emulator_top(rtl)"
        IO0_D17_N                 : inout std_logic; -- @suppress "Unused port: IO0_D17_N is not used in work.lti_emulator_top(rtl)"
        IO0_D18_P                 : inout std_logic; -- @suppress "Unused port: IO0_D18_P is not used in work.lti_emulator_top(rtl)"
        IO0_D19_N                 : inout std_logic; -- @suppress "Unused port: IO0_D19_N is not used in work.lti_emulator_top(rtl)"
        IO0_D20_P                 : inout std_logic; -- @suppress "Unused port: IO0_D20_P is not used in work.lti_emulator_top(rtl)"
        IO0_D21_N                 : inout std_logic; -- @suppress "Unused port: IO0_D21_N is not used in work.lti_emulator_top(rtl)"
        IO0_D22_P                 : inout std_logic; -- @suppress "Unused port: IO0_D22_P is not used in work.lti_emulator_top(rtl)"
        IO0_D23_N                 : inout std_logic; -- @suppress "Unused port: IO0_D23_N is not used in work.lti_emulator_top(rtl)"
        IO0_CLK1_N                : inout std_logic; -- @suppress "Unused port: IO0_CLK1_N is not used in work.lti_emulator_top(rtl)"
        IO0_CLK0_P                : inout std_logic; -- @suppress "Unused port: IO0_CLK0_P is not used in work.lti_emulator_top(rtl)"

        -- Anios_1
        IO1_D0_P                  : inout std_logic; -- @suppress "Unused port: IO1_D0_P is not used in work.lti_emulator_top(rtl)"
        IO1_D1_N                  : inout std_logic; -- @suppress "Unused port: IO1_D1_N is not used in work.lti_emulator_top(rtl)"
        IO1_D2_P                  : inout std_logic; -- @suppress "Unused port: IO1_D2_P is not used in work.lti_emulator_top(rtl)"
        IO1_D3_N                  : inout std_logic; -- @suppress "Unused port: IO1_D3_N is not used in work.lti_emulator_top(rtl)"
        IO1_D4_P                  : inout std_logic; -- @suppress "Unused port: IO1_D4_P is not used in work.lti_emulator_top(rtl)"
        IO1_D5_N                  : inout std_logic; -- @suppress "Unused port: IO1_D5_N is not used in work.lti_emulator_top(rtl)"
        IO1_D6_P                  : inout std_logic; -- @suppress "Unused port: IO1_D6_P is not used in work.lti_emulator_top(rtl)"
        IO1_D7_N                  : inout std_logic; -- @suppress "Unused port: IO1_D7_N is not used in work.lti_emulator_top(rtl)"
        IO1_D8_P                  : inout std_logic; -- @suppress "Unused port: IO1_D8_P is not used in work.lti_emulator_top(rtl)"
        IO1_D9_N                  : inout std_logic; -- @suppress "Unused port: IO1_D9_N is not used in work.lti_emulator_top(rtl)"
        IO1_D10_P                 : inout std_logic; -- @suppress "Unused port: IO1_D10_P is not used in work.lti_emulator_top(rtl)"
        IO1_D11_N                 : inout std_logic; -- @suppress "Unused port: IO1_D11_N is not used in work.lti_emulator_top(rtl)"
        IO1_D12_P                 : inout std_logic; -- @suppress "Unused port: IO1_D12_P is not used in work.lti_emulator_top(rtl)"
        IO1_D13_N                 : inout std_logic; -- @suppress "Unused port: IO1_D13_N is not used in work.lti_emulator_top(rtl)"
        IO1_D14_P                 : inout std_logic; -- @suppress "Unused port: IO1_D14_P is not used in work.lti_emulator_top(rtl)"
        IO1_D15_N                 : inout std_logic; -- @suppress "Unused port: IO1_D15_N is not used in work.lti_emulator_top(rtl)"
        IO1_D16_P                 : inout std_logic; -- @suppress "Unused port: IO1_D16_P is not used in work.lti_emulator_top(rtl)"
        IO1_D17_N                 : inout std_logic; -- @suppress "Unused port: IO1_D17_N is not used in work.lti_emulator_top(rtl)"
        IO1_D18_P                 : inout std_logic; -- @suppress "Unused port: IO1_D18_P is not used in work.lti_emulator_top(rtl)"
        IO1_D19_N                 : inout std_logic; -- @suppress "Unused port: IO1_D19_N is not used in work.lti_emulator_top(rtl)"
        IO1_D20_P                 : inout std_logic; -- @suppress "Unused port: IO1_D20_P is not used in work.lti_emulator_top(rtl)"
        IO1_D21_N                 : inout std_logic; -- @suppress "Unused port: IO1_D21_N is not used in work.lti_emulator_top(rtl)"
        IO1_D22_P                 : inout std_logic; -- @suppress "Unused port: IO1_D22_P is not used in work.lti_emulator_top(rtl)"
        IO1_D23_N                 : inout std_logic; -- @suppress "Unused port: IO1_D23_N is not used in work.lti_emulator_top(rtl)"
        IO1_CLK1_N                : inout std_logic; -- @suppress "Unused port: IO1_CLK1_N is not used in work.lti_emulator_top(rtl)"
        IO1_CLK0_P                : inout std_logic; -- @suppress "Unused port: IO1_CLK0_P is not used in work.lti_emulator_top(rtl)"

        -- DP
        DP_HPD                    : in    std_logic;
        DP_AUX_IN                 : in    std_logic;
        DP_AUX_OE                 : out   std_logic;
        DP_AUX_OUT                : out   std_logic;
        -- FMC
        FMC_HA02_N                : inout std_logic; -- @suppress "Unused port: FMC_HA02_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA02_P                : inout std_logic; -- @suppress "Unused port: FMC_HA02_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA03_N                : inout std_logic; -- @suppress "Unused port: FMC_HA03_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA03_P                : inout std_logic; -- @suppress "Unused port: FMC_HA03_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA04_N                : inout std_logic; -- @suppress "Unused port: FMC_HA04_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA04_P                : inout std_logic; -- @suppress "Unused port: FMC_HA04_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA05_N                : inout std_logic; -- @suppress "Unused port: FMC_HA05_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA05_P                : inout std_logic; -- @suppress "Unused port: FMC_HA05_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA06_N                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA06_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA06_P                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA06_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA07_N                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA07_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA07_P                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA07_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA08_N                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA08_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA08_P                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA08_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA09_N                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA09_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA09_P                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA09_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA10_N                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA10_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA10_P                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA10_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA11_N                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA11_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA11_P                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA11_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA12_N                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA12_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA12_P                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA12_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA13_N                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA13_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA13_P                : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA13_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA02_N                : inout std_logic; -- @suppress "Unused port: FMC_LA02_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA02_P                : inout std_logic; -- @suppress "Unused port: FMC_LA02_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA03_N                : inout std_logic; -- @suppress "Unused port: FMC_LA03_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA03_P                : inout std_logic; -- @suppress "Unused port: FMC_LA03_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA04_N                : inout std_logic; -- @suppress "Unused port: FMC_LA04_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA04_P                : inout std_logic; -- @suppress "Unused port: FMC_LA04_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA05_N                : inout std_logic; -- @suppress "Unused port: FMC_LA05_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA05_P                : inout std_logic; -- @suppress "Unused port: FMC_LA05_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA06_N                : inout std_logic; -- @suppress "Unused port: FMC_LA06_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA06_P                : inout std_logic; -- @suppress "Unused port: FMC_LA06_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA07_N                : inout std_logic; -- @suppress "Unused port: FMC_LA07_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA07_P                : inout std_logic; -- @suppress "Unused port: FMC_LA07_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA08_N                : inout std_logic; -- @suppress "Unused port: FMC_LA08_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA08_P                : inout std_logic; -- @suppress "Unused port: FMC_LA08_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA09_N                : inout std_logic; -- @suppress "Unused port: FMC_LA09_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA09_P                : inout std_logic; -- @suppress "Unused port: FMC_LA09_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA10_N                : inout std_logic; -- @suppress "Unused port: FMC_LA10_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA10_P                : inout std_logic; -- @suppress "Unused port: FMC_LA10_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA11_N                : inout std_logic; -- @suppress "Unused port: FMC_LA11_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA11_P                : inout std_logic; -- @suppress "Unused port: FMC_LA11_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA12_N                : inout std_logic; -- @suppress "Unused port: FMC_LA12_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA12_P                : inout std_logic; -- @suppress "Unused port: FMC_LA12_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA13_N                : inout std_logic; -- @suppress "Unused port: FMC_LA13_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA13_P                : inout std_logic; -- @suppress "Unused port: FMC_LA13_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA14_N                : inout std_logic; -- @suppress "Unused port: FMC_LA14_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA14_P                : inout std_logic; -- @suppress "Unused port: FMC_LA14_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA15_N                : inout std_logic; -- @suppress "Unused port: FMC_LA15_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA15_P                : inout std_logic; -- @suppress "Unused port: FMC_LA15_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA16_N                : inout std_logic; -- @suppress "Unused port: FMC_LA16_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA16_P                : inout std_logic; -- @suppress "Unused port: FMC_LA16_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA19_N                : inout std_logic; -- @suppress "Unused port: FMC_LA19_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA19_P                : inout std_logic; -- @suppress "Unused port: FMC_LA19_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA20_N                : inout std_logic; -- @suppress "Unused port: FMC_LA20_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA20_P                : inout std_logic; -- @suppress "Unused port: FMC_LA20_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA21_N                : inout std_logic; -- @suppress "Unused port: FMC_LA21_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA21_P                : inout std_logic; -- @suppress "Unused port: FMC_LA21_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA22_N                : inout std_logic; -- @suppress "Unused port: FMC_LA22_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA22_P                : inout std_logic; -- @suppress "Unused port: FMC_LA22_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA23_N                : inout std_logic; -- @suppress "Unused port: FMC_LA23_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA23_P                : inout std_logic; -- @suppress "Unused port: FMC_LA23_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA24_N                : inout std_logic; -- @suppress "Unused port: FMC_LA24_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA24_P                : inout std_logic; -- @suppress "Unused port: FMC_LA24_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA25_N                : inout std_logic; -- @suppress "Unused port: FMC_LA25_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA25_P                : inout std_logic; -- @suppress "Unused port: FMC_LA25_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA26_N                : inout std_logic; -- @suppress "Unused port: FMC_LA26_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA26_P                : inout std_logic; -- @suppress "Unused port: FMC_LA26_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA27_N                : inout std_logic; -- @suppress "Unused port: FMC_LA27_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA27_P                : inout std_logic; -- @suppress "Unused port: FMC_LA27_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA28_N                : inout std_logic; -- @suppress "Unused port: FMC_LA28_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA28_P                : inout std_logic; -- @suppress "Unused port: FMC_LA28_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA29_N                : inout std_logic; -- @suppress "Unused port: FMC_LA29_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA29_P                : inout std_logic; -- @suppress "Unused port: FMC_LA29_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA30_N                : inout std_logic; -- @suppress "Unused port: FMC_LA30_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA30_P                : inout std_logic; -- @suppress "Unused port: FMC_LA30_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA31_N                : inout std_logic; -- @suppress "Unused port: FMC_LA31_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA31_P                : inout std_logic; -- @suppress "Unused port: FMC_LA31_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA32_N                : inout std_logic; -- @suppress "Unused port: FMC_LA32_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA32_P                : inout std_logic; -- @suppress "Unused port: FMC_LA32_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA33_N                : inout std_logic; -- @suppress "Unused port: FMC_LA33_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA33_P                : inout std_logic; -- @suppress "Unused port: FMC_LA33_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA00_CC_N             : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA00_CC_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA00_CC_P             : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA00_CC_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA01_CC_N             : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA01_CC_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA01_CC_P             : inout std_logic; -- Only available on Regular modules -- @suppress "Unused port: FMC_HA01_CC_P is not used in work.lti_emulator_top(rtl)"
        FMC_HA17_N                : inout std_logic; -- @suppress "Unused port: FMC_HA17_N is not used in work.lti_emulator_top(rtl)"
        FMC_HA17_P                : inout std_logic; -- @suppress "Unused port: FMC_HA17_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA00_CC_N             : inout std_logic; -- @suppress "Unused port: FMC_LA00_CC_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA00_CC_P             : inout std_logic; -- @suppress "Unused port: FMC_LA00_CC_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA01_CC_N             : inout std_logic; -- @suppress "Unused port: FMC_LA01_CC_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA01_CC_P             : inout std_logic; -- @suppress "Unused port: FMC_LA01_CC_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA17_CC_N             : inout std_logic; -- @suppress "Unused port: FMC_LA17_CC_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA17_CC_P             : inout std_logic; -- @suppress "Unused port: FMC_LA17_CC_P is not used in work.lti_emulator_top(rtl)"
        FMC_LA18_CC_N             : inout std_logic; -- @suppress "Unused port: FMC_LA18_CC_N is not used in work.lti_emulator_top(rtl)"
        FMC_LA18_CC_P             : inout std_logic; -- @suppress "Unused port: FMC_LA18_CC_P is not used in work.lti_emulator_top(rtl)"
        FMC_CLK0_M2C_N            : inout std_logic; -- @suppress "Unused port: FMC_CLK0_M2C_N is not used in work.lti_emulator_top(rtl)"
        FMC_CLK0_M2C_P            : inout std_logic; -- @suppress "Unused port: FMC_CLK0_M2C_P is not used in work.lti_emulator_top(rtl)"
        FMC_CLK1_M2C_N            : inout std_logic; -- @suppress "Unused port: FMC_CLK1_M2C_N is not used in work.lti_emulator_top(rtl)"
        FMC_CLK1_M2C_P            : inout std_logic; -- @suppress "Unused port: FMC_CLK1_M2C_P is not used in work.lti_emulator_top(rtl)"

        -- HDMI
        HDMI_HPD                  : inout std_logic; -- @suppress "Unused port: HDMI_HPD is not used in work.lti_emulator_top(rtl)"
        HDMI_CLK_N                : inout std_logic; -- @suppress "Unused port: HDMI_CLK_N is not used in work.lti_emulator_top(rtl)"
        HDMI_CLK_P                : inout std_logic; -- @suppress "Unused port: HDMI_CLK_P is not used in work.lti_emulator_top(rtl)"

        -- I2C_PL
        I2C_SCL_PL                : inout std_logic; -- @suppress "Unused port: I2C_SCL_PL is not used in work.lti_emulator_top(rtl)"
        I2C_SDA_PL                : inout std_logic; -- @suppress "Unused port: I2C_SDA_PL is not used in work.lti_emulator_top(rtl)"

        -- IO3
        IO3_D0_P                  : inout std_logic; -- @suppress "Unused port: IO3_D0_P is not used in work.lti_emulator_top(rtl)"
        IO3_D1_N                  : inout std_logic; -- @suppress "Unused port: IO3_D1_N is not used in work.lti_emulator_top(rtl)"
        IO3_D2_P                  : inout std_logic; -- @suppress "Unused port: IO3_D2_P is not used in work.lti_emulator_top(rtl)"
        IO3_D3_N                  : inout std_logic; -- @suppress "Unused port: IO3_D3_N is not used in work.lti_emulator_top(rtl)"
        IO3_D4_P                  : inout std_logic; -- @suppress "Unused port: IO3_D4_P is not used in work.lti_emulator_top(rtl)"
        IO3_D5_N                  : inout std_logic; -- @suppress "Unused port: IO3_D5_N is not used in work.lti_emulator_top(rtl)"
        IO3_D6_P                  : inout std_logic; -- @suppress "Unused port: IO3_D6_P is not used in work.lti_emulator_top(rtl)"
        IO3_D7_N                  : inout std_logic; -- @suppress "Unused port: IO3_D7_N is not used in work.lti_emulator_top(rtl)"

        -- IO4
        IO4_D2_P                  : in std_logic; 
        IO4_D3_N                  : inout std_logic; -- @suppress "Unused port: IO4_D3_N is not used in work.lti_emulator_top(rtl)"
        IO4_D4_P                  : inout std_logic; -- @suppress "Unused port: IO4_D4_P is not used in work.lti_emulator_top(rtl)"
        IO4_D5_N                  : inout std_logic; -- @suppress "Unused port: IO4_D5_N is not used in work.lti_emulator_top(rtl)"
        IO4_D6_P                  : inout std_logic; -- @suppress "Unused port: IO4_D6_P is not used in work.lti_emulator_top(rtl)"
        IO4_D7_N                  : inout std_logic; -- @suppress "Unused port: IO4_D7_N is not used in work.lti_emulator_top(rtl)"

        -- LED
        LED2_N_PWR_SYNC           : out   std_logic;
        -- MIPI0
        MIPI0_D0_N                : inout std_logic; -- @suppress "Unused port: MIPI0_D0_N is not used in work.lti_emulator_top(rtl)"
        MIPI0_D0_P                : inout std_logic; -- @suppress "Unused port: MIPI0_D0_P is not used in work.lti_emulator_top(rtl)"
        MIPI0_D1_N                : inout std_logic; -- @suppress "Unused port: MIPI0_D1_N is not used in work.lti_emulator_top(rtl)"
        MIPI0_D1_P                : inout std_logic; -- @suppress "Unused port: MIPI0_D1_P is not used in work.lti_emulator_top(rtl)"
        MIPI0_CLK_N               : inout std_logic; -- @suppress "Unused port: MIPI0_CLK_N is not used in work.lti_emulator_top(rtl)"
        MIPI0_CLK_P               : inout std_logic; -- @suppress "Unused port: MIPI0_CLK_P is not used in work.lti_emulator_top(rtl)"
        MIPI0_CLK_D0LP_N          : inout std_logic; -- @suppress "Unused port: MIPI0_CLK_D0LP_N is not used in work.lti_emulator_top(rtl)"
        MIPI0_CLK_D0LP_P          : inout std_logic; -- @suppress "Unused port: MIPI0_CLK_D0LP_P is not used in work.lti_emulator_top(rtl)"

        -- MIPI1
        MIPI1_D0_N                : inout std_logic; -- @suppress "Unused port: MIPI1_D0_N is not used in work.lti_emulator_top(rtl)"
        MIPI1_D0_P                : inout std_logic; -- @suppress "Unused port: MIPI1_D0_P is not used in work.lti_emulator_top(rtl)"
        MIPI1_D1_N                : inout std_logic; -- @suppress "Unused port: MIPI1_D1_N is not used in work.lti_emulator_top(rtl)"
        MIPI1_D1_P                : inout std_logic; -- @suppress "Unused port: MIPI1_D1_P is not used in work.lti_emulator_top(rtl)"
        MIPI1_CLK_N               : inout std_logic; -- @suppress "Unused port: MIPI1_CLK_N is not used in work.lti_emulator_top(rtl)"
        MIPI1_CLK_P               : inout std_logic; -- @suppress "Unused port: MIPI1_CLK_P is not used in work.lti_emulator_top(rtl)"
        MIPI1_CLK_D0LP_N          : inout std_logic; -- @suppress "Unused port: MIPI1_CLK_D0LP_N is not used in work.lti_emulator_top(rtl)"
        MIPI1_CLK_D0LP_P          : inout std_logic; -- @suppress "Unused port: MIPI1_CLK_D0LP_P is not used in work.lti_emulator_top(rtl)"

        -- OSC_100M
        CLK_100_CAL               : in    std_logic; -- @suppress "Unused port: CLK_100_CAL is not used in work.lti_emulator_top(rtl)"

        -- user i/o
        busy_in                   : in    std_logic; -- @suppress "Unused port: busy_in is not used in work.lti_emulator_top(rtl)"
        Q0_CLK1_GTREFCLK_PAD_N_IN : in    std_logic;
        Q0_CLK1_GTREFCLK_PAD_P_IN : in    std_logic;
        FMC_REFCLK_N_IN           : in    std_logic;
        FMC_REFCLK_P_IN           : in    std_logic;
        gtxtxn_out                : out   std_logic_vector(4 downto 0);
        gtxtxp_out                : out   std_logic_vector(4 downto 0);
        gtxrxn_in                 : in    std_logic_vector(4 downto 0);
        gtxrxp_in                 : in    std_logic_vector(4 downto 0);
        rd_en_pb                  : in    std_logic := '0'; -- @suppress "Unused port: rd_en_pb is not used in work.lti_emulator_top(rtl)"
        wr_en_pb                  : in    std_logic := '0'; -- @suppress "Unused port: wr_en_pb is not used in work.lti_emulator_top(rtl)"
 
        -- IO3 (Left)
        PCLK_IO3_PIN1_D15         : out   std_logic; -- IO3_D0_P
        IO3_PIN3                  : out   std_logic;
        IO3_PIN5                  : out   std_logic;
        -- IO3 (Right)
        PCLK_IO3_PIN2_B13         : out   std_logic;
        --IO3_D1_N         : in    std_logic;
        LED2                      : out   std_logic
    );
end lti_emulator_top;

architecture rtl of lti_emulator_top is
    ---------------------------------------------------------------------------------------------------
    -- signal declarations
    ---------------------------------------------------------------------------------------------------
    signal Clk100             : std_logic;
    signal Rst_N              : std_logic;
    signal dp_aux_data_oe_n   : std_logic;
    signal LedCount           : std_logic_vector(24 downto 0);
    signal addrb_0            : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal dinb_0             : STD_LOGIC_VECTOR(31 downto 0) := (others => '0'); -- @suppress "signal dinb_0 is never written"
    signal doutb_0            : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal enb_0              : STD_LOGIC                     := '0';
    signal web_0              : STD_LOGIC_VECTOR(3 DOWNTO 0)  := (others => '0');
    signal BCR_PERIOD_out_0   : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal BUSY_PULSE_COUNT_0 : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal L1A_COUNT_0        : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal L1A_COUNT_out_0    : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal L1A_PERIOD_out_0   : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal OCR_COUNT_0        : STD_LOGIC_VECTOR(31 downto 0) := (others => '0'); -- @suppress "signal OCR_COUNT_0 is never written"
    signal OCR_PERIOD_out_0   : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal TTC_CONFIG_out_0   : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal BUSY_EN_0          : STD_LOGIC                     := '0';
    signal BUSY_in_0          : STD_LOGIC                     := '0';
    signal ECR_out_0          : STD_LOGIC                     := '0';
    signal L1A_AUTO_HALT_0    : STD_LOGIC                     := '0';
    signal L1A_AUTO_READY_0   : STD_LOGIC                     := '0';
    signal L1A_out_0          : STD_LOGIC                     := '0';
    signal LUT_CONT_EN_0      : STD_LOGIC                     := '0';

    signal BCR_count_en : std_logic;
    signal ECR_r        : std_logic;
    signal trigger_r    : std_logic;
    signal BCR_r        : std_logic;
    signal TXUSRCLK_OUT : std_logic;
    signal clk40        : std_logic;
    signal clk40_ddr    : std_logic;
    signal RXUSRCLK_OUT : std_logic_vector(4 downto 0);

    signal Rst                           : std_logic := '0';
    signal LTIData_out                   : array_std_logic_vector32_type;
    signal rxdata_out                    : array_std_logic_vector16_type;
    signal Kchar_rx                      : array_std_logic_vector2_type;
    signal LTIData_out_t                 : std_logic_vector(31 downto 0);
    signal Kchar_out                     : array_std_logic_vector4_type;
    signal Kchar_out_t                   : std_logic_vector(3 downto 0);
    --LTI interface
    signal BCR_PERIOD_COUNT              : std_logic_vector(11 downto 0);
    signal L1A_COUNT                     : std_logic_vector(37 downto 0);
    signal ORBITID_COUNT                 : std_logic_vector(31 downto 0);
    signal LBID_0                        : STD_LOGIC_VECTOR(15 downto 0);
    signal GRST_0                        : STD_LOGIC;
    signal SL0ID_0                       : STD_LOGIC;
    signal SORB_0                        : STD_LOGIC;
    signal SYNC_0                        : STD_LOGIC;
    signal TRIGGERTYPE_0                 : STD_LOGIC_VECTOR(15 downto 0);
    signal ERRORFLAGS_0                  : STD_LOGIC_VECTOR(3 downto 0);
    signal TS_0                          : STD_LOGIC;
    signal SYNCGLOBALDATA_0              : STD_LOGIC_VECTOR(15 downto 0);
    signal SYNCUSERDATA_0                : STD_LOGIC_VECTOR(15 downto 0);
    signal PARTITION_0                   : STD_LOGIC_VECTOR(1 downto 0);
    signal PT_0                          : STD_LOGIC;
    signal reset_gth_0                   : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal busy                          : std_logic_vector(4 downto 0);
    signal TX_DISABLE_0                  : std_logic_vector(4 downto 0);
    signal BUSY_ENABLE_0                 : std_logic_vector(4 downto 0);
    signal BUSY_in_0_100                 : std_logic;
    signal L1A_COUNT_0_100               : std_logic_vector(31 downto 0);
    signal L1A_AUTO_READY_0_100          : STD_LOGIC;
    signal BUSY_PULSE_COUNT_0_100        : std_logic_vector(31 downto 0);
    signal L1A_PERIOD_out_0_TXUSRCLK_OUT : std_logic_vector(31 downto 0);
    signal BCR_PERIOD_out_0_TXUSRCLK_OUT : std_logic_vector(31 downto 0);
    signal L1A_COUNT_out_0_TXUSRCLK_OUT  : std_logic_vector(31 downto 0);
    signal L1A_AUTO_HALT_TXUSRCLK_OUT    : STD_LOGIC;
    signal BUSY_EN_0_txuserclk_out       : std_logic;
    signal LUT_CONT_EN_0_TXUSRCLK_OUT    : STD_LOGIC;
    signal TTC_CONFIG_out_0_TXUSRCLK_OUT : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal LBID_0_TXUSRCLK_OUT           : std_logic_VECTOR(15 DOWNTO 0);
    signal SORB_0_TXUSRCLK_OUT           : std_logic;
    signal SYNCGLOBALDATA_0_TXUSRCLK_OUT : std_logic_VECTOR(15 DOWNTO 0);
    signal SYNCUSERDATA_0_TXUSRCLK_OUT   : std_logic_VECTOR(15 DOWNTO 0);
    signal ERRORFLAGS_0_TXUSRCLK_OUT     : std_logic_VECTOR(3 DOWNTO 0);
    signal PARTITION_0_TXUSRCLK_OUT      : std_logic_VECTOR(1 DOWNTO 0);
    signal GRST_0_TXUSRCLK_OUT           : std_logic;
    signal SYNC_0_TXUSRCLK_OUT           : std_logic;
    signal TRIGGERTYPE_0_TXUSRCLK_OUT    : std_logic_VECTOR(15 DOWNTO 0);
    signal PT_0_TXUSRCLK_OUT             : std_logic;
    signal Q0_CLK0_GTREFCLK_PAD_div2     : std_logic;
    signal L0A                           : std_logic;
    signal GT_REFCLK_MAIN_p : std_logic;
    signal GT_REFCLK_MAIN_N : std_logic;
    signal GT_REFCLK_2_p : std_logic;
    signal GT_REFCLK_2_N : std_logic;
    signal GTH_refclk_sel : std_logic;
   
begin

    ---------------------------------------------------------------------------------------------------
    -- processor system instance
    ---------------------------------------------------------------------------------------------------
    edge_det_inst : entity work.edge_det
        port map(
            clk_160              => TXUSRCLK_OUT,
            sys_rst_n            => Rst_N,
            ECR_push_in          => ECR_out_0,
            trigger_push_in      => L1A_out_0 or (not IO4_D2_P),
            busy_in              => Rst_N,
            SORB_in              => SORB_0_TXUSRCLK_OUT,
            BCR_count_en_in      => BCR_count_en,
            BCR_PERIOD_IN        => BCR_PERIOD_out_0_TXUSRCLK_OUT,
            L1A_COUNT_IN         => L1A_COUNT_out_0_TXUSRCLK_OUT,
            L1A_PERIOD_IN        => L1A_PERIOD_out_0_TXUSRCLK_OUT,
            TTC_CONFIG_IN        => TTC_CONFIG_out_0_TXUSRCLK_OUT,
            LUT_CONT_EN_in       => LUT_CONT_EN_0_TXUSRCLK_OUT,
            L1A_AUTO_HALT_in     => L1A_AUTO_HALT_TXUSRCLK_OUT,
            BUSY_EN_in           => BUSY_EN_0_txuserclk_out,
            doutb_in             => doutb_0,
            ECR_out              => ECR_r,
            trigger_out          => trigger_r,
            BCR_out              => BCR_r,
            L1A_COUNT_read_out   => L1A_COUNT_0,
            L1A_AUTO_READY_out   => L1A_AUTO_READY_0,
            BUSY_PULSE_COUNT_out => BUSY_PULSE_COUNT_0,
            enb_out              => enb_0,
            web_out              => web_0,
            addrb_out            => addrb_0,
            BCR_PERIOD_COUNT_OUT => BCR_PERIOD_COUNT,
            L1A_COUNT_OUT        => L1A_COUNT,
            ORBITID_COUNT_OUT    => ORBITID_COUNT
        );

    config_gen_inst : entity work.configuration_cdc
        port map(
            TXUSRCLK                    => TXUSRCLK_OUT,
            CLK_100                     => Clk100,
            BCR_PERIOD_in               => BCR_PERIOD_out_0,
            L1A_PERIOD_in               => L1A_PERIOD_out_0,
            L1A_COUNT_in                => L1A_COUNT_out_0,
            BUSY_PULSE_COUNT_in         => BUSY_PULSE_COUNT_0,
            L1A_COUNT_0_in              => L1A_COUNT_0,
            OCR_PERIOD_in               => OCR_PERIOD_out_0,
            L1A_AUTO_HALT_in            => L1A_AUTO_HALT_0,
            BUSY_EN_in                  => BUSY_EN_0,
            LUT_CONT_EN_in              => LUT_CONT_EN_0,
            TTC_CONFIG_in               => TTC_CONFIG_out_0,
            SYNCUSERDATA_in             => SYNCUSERDATA_0,
            SORB_in                     => SORB_0,
            ERRORFLAGS_in               => ERRORFLAGS_0,
            LBID_in                     => LBID_0,
            PARTITION_in                => PARTITION_0,
            SYNCGLOBALDATA_in           => SYNCGLOBALDATA_0,
            GRST_in                     => GRST_0,
            SYNC_in                     => SYNC_0,
            TRIGGERTYPE_in              => TRIGGERTYPE_0,
            PT_in                       => PT_0,
            BCR_PERIOD_TXUSRCLK_OUT     => BCR_PERIOD_out_0_TXUSRCLK_OUT,
            L1A_PERIOD_TXUSRCLK_OUT     => L1A_PERIOD_out_0_TXUSRCLK_OUT,
            L1A_COUNT_TXUSRCLK_OUT      => L1A_COUNT_out_0_TXUSRCLK_OUT,
            BUSY_PULSE_COUNT_100_out    => BUSY_PULSE_COUNT_0_100,
            L1A_COUNT_100_out           => L1A_COUNT_0_100,
            OCR_PERIOD_TXUSRCLK_OUT     => open, -- TODO : FIXME
            L1A_AUTO_HALT_TXUSRCLK_OUT  => L1A_AUTO_HALT_TXUSRCLK_OUT,
            BUSY_EN_txuserclk_out       => BUSY_EN_0_txuserclk_out,
            LUT_CONT_EN_TXUSRCLK_OUT    => LUT_CONT_EN_0_TXUSRCLK_OUT,
            TTC_CONFIG_TXUSRCLK_OUT     => TTC_CONFIG_out_0_TXUSRCLK_OUT,
            SYNCUSERDATA_TXUSRCLK_OUT   => SYNCUSERDATA_0_TXUSRCLK_OUT,
            SORB_TXUSRCLK_OUT           => SORB_0_TXUSRCLK_OUT,
            ERRORFLAGS_TXUSRCLK_OUT     => ERRORFLAGS_0_TXUSRCLK_OUT,
            LBID_TXUSRCLK_OUT           => LBID_0_TXUSRCLK_OUT,
            PARTITION_TXUSRCLK_OUT      => PARTITION_0_TXUSRCLK_OUT,
            SYNCGLOBALDATA_TXUSRCLK_OUT => SYNCGLOBALDATA_0_TXUSRCLK_OUT,
            GRST_TXUSRCLK_OUT           => GRST_0_TXUSRCLK_OUT,
            SYNC_TXUSRCLK_OUT           => SYNC_0_TXUSRCLK_OUT,
            TRIGGERTYPE_TXUSRCLK_OUT    => TRIGGERTYPE_0_TXUSRCLK_OUT,
            PT_TXUSRCLK_OUT             => PT_0_TXUSRCLK_OUT
        );

    data_gen_inst : entity work.LTI_DATA_GEN
        port map(
            clk                 => TXUSRCLK_OUT,
            ECR_in              => ECR_r,
            trigger_in          => trigger_r,
            BCR_in              => BCR_r,
            BCR_PERIOD_COUNT_IN => BCR_PERIOD_COUNT,
            L1A_COUNT_IN        => L1A_COUNT,
            ORBITID_COUNT_IN    => ORBITID_COUNT,
            LBID_in             => LBID_0_TXUSRCLK_OUT,
            GRST_in             => GRST_0_TXUSRCLK_OUT,
            SL0ID_in            => SL0ID_0,
            SORB_in             => SORB_0_TXUSRCLK_OUT,
            SYNC_in             => SYNC_0_TXUSRCLK_OUT,
            TRIGGERTYPE_in      => TRIGGERTYPE_0_TXUSRCLK_OUT,
            ERRORFLAGS_in       => ERRORFLAGS_0_TXUSRCLK_OUT,
            TS_in               => TS_0,
            SYNCGLOBALDATA_in   => SYNCGLOBALDATA_0_TXUSRCLK_OUT,
            SYNCUSERDATA_in     => SYNCUSERDATA_0_TXUSRCLK_OUT,
            PARTITION_in        => PARTITION_0_TXUSRCLK_OUT,
            PT_in               => PT_0_TXUSRCLK_OUT,
            BCR_count_en_out    => BCR_count_en,
            LTIData_out         => LTIData_out_t,
            Kchar_out           => Kchar_out_t,
            L0A_out             => L0A
        );

    Mercury_XU1_inst : entity work.Mercury_XU1_wrapper
        port map(
            BCR_PERIOD_out_0   => BCR_PERIOD_out_0,
            BCR_out_0          => open, --TODO: Fix me
            BUSY_ENABLE_0      => BUSY_ENABLE_0,
            BUSY_EN_0          => BUSY_EN_0,
            BUSY_PULSE_COUNT_0 => BUSY_PULSE_COUNT_0_100,
            BUSY_in_0          => BUSY_in_0_100,
            Clk100             => Clk100,
            DP_AUX_IN          => DP_AUX_IN,
            DP_AUX_OE          => dp_aux_data_oe_n,
            DP_AUX_OUT         => DP_AUX_OUT,
            DP_HPD             => DP_HPD,
            ECR_out_0          => ECR_out_0,
            ERRORFLAGS_0       => ERRORFLAGS_0,
            GRST_0             => GRST_0,
            L1A_AUTO_HALT_0    => L1A_AUTO_HALT_0,
            L1A_AUTO_READY_0   => L1A_AUTO_READY_0_100,
            L1A_COUNT_0        => L1A_COUNT_0_100,
            L1A_COUNT_out_0    => L1A_COUNT_out_0,
            L1A_PERIOD_out_0   => L1A_PERIOD_out_0,
            L1A_out_0          => L1A_out_0,
            LBID_0             => LBID_0,
            LUT_CONT_EN_0      => LUT_CONT_EN_0,
            OCR_COUNT_0        => OCR_COUNT_0,
            OCR_PERIOD_out_0   => OCR_PERIOD_out_0,
            PARTITION_0        => PARTITION_0,
            PT_0               => PT_0,
            Rst_N              => Rst_N,
            SL0ID_0            => SL0ID_0,
            SORB_0             => SORB_0,
            SYNCGLOBALDATA_0   => SYNCGLOBALDATA_0,
            SYNCUSERDATA_0     => SYNCUSERDATA_0,
            SYNC_0             => SYNC_0,
            TRIGGERTYPE_0      => TRIGGERTYPE_0,
            TS_0               => TS_0,
            TTC_CONFIG_out_0   => TTC_CONFIG_out_0,
            TX_DISABLE_0       => TX_DISABLE_0,
            addrb_0            => addrb_0,
            clkb_0             => TXUSRCLK_OUT,
            dinb_0             => dinb_0,
            doutb_0            => doutb_0,
            enb_0              => enb_0,
            reset_gth_0        => reset_gth_0,
            rstb_0             => '0',
            web_0              => web_0,
            CLK_SEL_0          => GTH_refclk_sel           
        );

    xpm_cdc_single_L1A_AUTO_READY : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => L1A_AUTO_READY_0, -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => Clk100,         -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => L1A_AUTO_READY_0_100 -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );

    xpm_cdc_single_BUSY_IN : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 4,        -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,        -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0         -- DECIMAL; 0=do not register input, 1=register input
        )
        port map(
            src_clk  => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => BUSY_in_0,      -- 1-bit input: Input signal to be synchronized to dest_clk domain.
            -- is registered.

            dest_clk => Clk100,         -- 1-bit input: Clock signal for the destination clock domain.
            dest_out => BUSY_in_0_100   -- 1-bit output: src_in synchronized to the destination clock domain. This output
        );

    BUSY_in_0 <= (busy(4) and BUSY_ENABLE_0(4)) OR (busy(3) and BUSY_ENABLE_0(3)) OR (busy(2) and BUSY_ENABLE_0(2)) OR (busy(1) and BUSY_ENABLE_0(1)) OR (busy(0) and BUSY_ENABLE_0(0));

    g_main_refclk: if G_MAIN_REFCLK_SEL = '1' generate -- FMC refclock
        GT_REFCLK_MAIN_p <= FMC_REFCLK_P_IN;
        GT_REFCLK_MAIN_N <= FMC_REFCLK_N_IN;
        GT_REFCLK_2_N <= Q0_CLK1_GTREFCLK_PAD_N_IN;
        GT_REFCLK_2_p <= Q0_CLK1_GTREFCLK_PAD_P_IN;
    else generate 
        GT_REFCLK_MAIN_p <= Q0_CLK1_GTREFCLK_PAD_P_IN;
        GT_REFCLK_MAIN_N <= Q0_CLK1_GTREFCLK_PAD_N_IN;
        GT_REFCLK_2_N <= FMC_REFCLK_N_IN;
        GT_REFCLK_2_p <=FMC_REFCLK_P_IN;
    end generate;

    TTC_ZYNQUltraScale_WRAPPER_inst : entity work.TTC_ZYNQUltraScale_WRAPPER
        port map(
            CLK_100_CAL            => Clk100,
            gtxtxn_out             => gtxtxn_out,
            gtxtxp_out             => gtxtxp_out,
            gtxrxp_in              => gtxrxp_in,
            gtxrxn_in              => gtxrxn_in,
            rxdata_out             => rxdata_out,
            Kchar_rx               => Kchar_rx,
            txdata_in              => LTIData_out,
            Kchar_tx               => Kchar_out,
            RXUSRCLK_OUT           => RXUSRCLK_OUT,
            TXUSRCLK_OUT           => TXUSRCLK_OUT,
            TX_DISABLE             => TX_DISABLE_0,
            reset_in               => reset_gth_0,
            Q0_CLK0_GTREFCLK_PAD_P => GT_REFCLK_MAIN_p,
            Q0_CLK0_GTREFCLK_PAD_N => GT_REFCLK_MAIN_N,
            Q0_CLK1_GTREFCLK_PAD_P => GT_REFCLK_2_p,
            Q0_CLK1_GTREFCLK_PAD_N => GT_REFCLK_2_N,

            Q0_CLK0_GTREFCLK_PAD_div2 => Q0_CLK0_GTREFCLK_PAD_div2,
            CLK_40_out             => clk40,
	    GTH_refclk_sel_in      => GTH_refclk_sel
        );

    data_rcv_inst : for i in 0 to 4 generate
        LTIData_out(i) <= LTIData_out_t;
        Kchar_out(i)   <= Kchar_out_t;

        LTI_DATA_RCV_inst : entity work.LTI_DATA_RCV
            port map(
                clk240_in  => RXUSRCLK_OUT(i),
                reset_in   => Rst,
                LTIData_in => rxdata_out(i),
                Kchar_in   => Kchar_rx(i),
                busy       => busy(i)
            );
    end generate;
    DP_AUX_OE <= not dp_aux_data_oe_n;

    Rst             <= not Rst_N;
    process(Clk100)
    begin
        if rising_edge(Clk100) then
            if Rst_N = '0' then
                LedCount <= (others => '0');
            else
                LedCount <= LedCount + 1;
            end if;
        end if;
    end process;
    
ODDRE1_inst : ODDRE1
generic map (
   IS_C_INVERTED => '0',            -- Optional inversion for C
   IS_D1_INVERTED => '0',           -- Unsupported, do not use
   IS_D2_INVERTED => '0',           -- Unsupported, do not use
   SIM_DEVICE => "ULTRASCALE_PLUS", -- Set the device version for simulation functionality (ULTRASCALE,
                                    -- ULTRASCALE_PLUS, ULTRASCALE_PLUS_ES1, ULTRASCALE_PLUS_ES2)
   SRVAL => '0'                     -- Initializes the ODDRE1 Flip-Flops to the specified value ('0', '1')
)
port map (
   Q => clk40_ddr,   -- 1-bit output: Data output to IOB
   C => clk40,   -- 1-bit input: High-speed clock input
   D1 => '1', -- 1-bit input: Parallel data input 1
   D2 => '0', -- 1-bit input: Parallel data input 2
   SR => '0'  -- 1-bit input: Active-High Async Reset
);
 
    LED2_N_PWR_SYNC <= '0' when LedCount(LedCount'high) = '0' else 'Z';
    LED2            <= not IO4_D2_P;
    
    -- Output connector (Left)
    PCLK_IO3_PIN1_D15 <= TXUSRCLK_OUT;
    IO3_PIN3 <= clk40_ddr;

    IO3_PIN5 <= L0A;
    -- Output connector (Right)
    PCLK_IO3_PIN2_B13 <= Q0_CLK0_GTREFCLK_PAD_div2;
    
    FMC_LA03_P <= '0'; -- FMC CLK_sel
    FMC_LA03_N <= '0'; -- FMC SI570 OE
end rtl;
