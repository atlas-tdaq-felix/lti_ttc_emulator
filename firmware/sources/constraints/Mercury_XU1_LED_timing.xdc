create_clock -period 6.400 -name SI570_REFCLK [get_ports Q0_CLK1_GTREFCLK_PAD_P_IN]
create_clock -period 6.400 -name FMC_REFCLK [get_ports FMC_REFCLK_P_IN]

create_clock -period 10.000 -name CLK_100_CAL [get_ports CLK_100_CAL]

set_max_delay -from [get_pins {Mercury_XU1_inst/Mercury_XU1_i/lti_registers_0/U0/lti_registers_v1_0_S00_AXI_inst/reset_gth_reg_reg[0]/C}] -to [get_pins TTC_ZYNQUltraScale_WRAPPER_inst/BUFGCE_DIV_inst_40/CLR] 4.150

set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets Clk100]
