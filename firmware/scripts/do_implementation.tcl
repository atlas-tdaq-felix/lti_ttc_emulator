#
# Runs implementation
# 
# 
# 
# ########################################################################################

set G_MAIN_REFCLK_SEL 1'b0

set_property generic "G_MAIN_REFCLK_SEL=$G_MAIN_REFCLK_SEL" [current_fileset]


if {[file exists scripts/settings.tcl] } { source scripts/settings.tcl }
if {![info exists vivado_dir]} { set vivado_dir "Vivado/${module_name}" }

#Copy back all the IP cores from the project back into the repository
set IPS [get_ips -exclude_bd_ips]
set XCI_FILES ""
foreach IP $IPS {
    set XCI_FILES [concat $XCI_FILES " " [get_files $IP.xci]]
}
#set XCI_FILES [get_files *.xci]
set BD_FILES [get_files *.bd]

set core_dir sources/ip/


foreach XCI_FILE $XCI_FILES {
  file copy -force $XCI_FILE $core_dir
}

foreach BD_FILE $BD_FILES {
  file copy -force $BD_FILE $core_dir
}


reset_run synth_1
launch_runs impl_1 -to_step write_bitstream -jobs 12

wait_on_runs impl_1

write_hw_platform -fixed -force -file ${vivado_dir}/lti_emulator_top.xsa
file copy -force ${vivado_dir}/Mercury_XU1_ST1.runs/impl_1/lti_emulator_top.bit ${vivado_dir}/bit.bit


puts "INFO: Done!"
